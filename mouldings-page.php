<?php /*Template Name: Mouldings*/?>

<?php get_header(); ?>

<!-- Featured Background-->
		<div id="top"></div>
		<div class="fullscreen" id="mouldings-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <h1 class="featured-title italic">Mouldings</h1>
        <p>Enhance the style and design of any space with our striking mouldings. We have a wide range of moulding profiles made from a variety of domestic and exotic hardwoods.</p>
        <p>To see examples of our custom mouldings, click <a href="#mouldingCarousel" class="bold">here.</a></p>
		<div class="scrolling-arrow">
        	<p class="center"><a href="#scrolling-content">Scroll Down</a></p>
        	<a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
        </div>
        </div>
        </div>
        </div>
        
        <div id="scrolling-content"></div>

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>314.781.5151</ph>
            <p class="italic">Interested in custom moulding? </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an Estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
		<div id="mouldings">
        <div class="container">
        <div class="intro-text">
        <h2>Moulding Profiles for Every Taste and Space</h2>
        <p>Our mouldings are made from strong, quality wood, and our craftsmen follow detailed and precise carving and manufacturing processes. Stocking profiles include Poplar and Red Oak, Walnut, Mahogany, Cherry, Birch, Oak, Maple, Pine, Brazilian and American Cherry, Alder and many other species and sizes in our full mouldings line. To learn more, download a PDF of our catalog. </p>
        <p>All profiles can be transformed into arched casings, curved crowns and even carvings for virtually any millwork product you are looking for to enhance your space. We also match existing profiles and create new profiles with in-house knife grinding.</p>
        </div>
        <div class="intro-text">
        <a href="http://resources.kstair.com/mouldings/"><img src="<?php bloginfo('template_directory'); ?>/images/moulding-catalog-btn.svg"></a>
        </div>
         <div class="icon-callouts">
        	<div>
        	<a href="http://www.pinterest.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/pinterest-icon.svg" alt="Follow us on Pinterest"></a>
        	<p class="bold"><a href="http://www.pinterest.com/kirkwoodstair/" target="_blank">Follow us on Pinterest to see custom moulding and more.</a></p>
            </div>
        </div>
        </div>
        </div>
        </div>
        
    <div id="mouldingCarousel" class="carousel slide">
    <!--<ol class="carousel-indicators">
    <li data-target="#countertopCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#countertopCarousel" data-slide-to="1"></li>
    <li data-target="#countertopCarousel" data-slide-to="2"></li>
    </ol>-->
    <!-- Carousel items -->
    <div class="carousel-inner">
    <div class="active item">
        <picture>
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsA-l-min.jpg" media="(min-width: 1201px)" />
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsA-m-min.jpg" media="(min-width: 601px)" />
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsA-s-min.jpg" media="(max-width: 600px)" />
            <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsA-l-min.jpg" />
        </picture>
    </div>
    <div class="item">
        <picture>
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsB-l-min.jpg" media="(min-width: 1201px)" />
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsB-m-min.jpg" media="(min-width: 601px)" />
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsB-s-min.jpg" media="(max-width: 600px)" />
            <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsB-l-min.jpg" />
        </picture>
    </div>
    <div class="item">
        <picture>
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsC-l-min.jpg" media="(min-width: 1201px)" />
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsC-m-min.jpg" media="(min-width: 601px)" />
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsC-s-min.jpg" media="(max-width: 600px)" />
            <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsC-l-min.jpg" />
        </picture>
    </div>
    <div class="item">
        <picture>
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsD-l-min.jpg" media="(min-width: 1201px)" />
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsD-m-min.jpg" media="(min-width: 601px)" />
            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsD-s-min.jpg" media="(max-width: 600px)" />
            <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsD-l-min.jpg" />
        </picture>
    </div>
    </div>
    <!-- Carousel nav -->
    <a class="carousel-control left" href="#mouldingCarousel" data-slide="prev"><img src="<?php bloginfo('template_directory'); ?>/images/pg-left.svg"></a>
    <a class="carousel-control right" href="#mouldingCarousel" data-slide="next"><img src="<?php bloginfo('template_directory'); ?>/images/pg-right.svg"></a>
    </div>
    
    	<div class="call-out-bar">
        <p><a href="/contact">Interested in custom moulding? Click here for an estimate.</a></p>
        </div>
        
        
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
