<?php /*Template Name: Resources*/?>

<?php get_header(); ?>

<!-- Featured Background-->
		<div id="top"></div>
		<div class="fullscreen" id="resource-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <h1 class="featured-title italic">Resources</h1>
        <p>Log-in below to access CAD files and additional assets.</p>
		<div class="scrolling-arrow">
        	<p class="center"><a href="#scrolling-content">Scroll Down</a></p>
        	<a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
        </div>
        </div>
        </div>
        </div>
        
        <div id="scrolling-content"></div>

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>314.781.5151</ph>
            <p class="italic">Contact us with any questions and comments. </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
        <div id="resource-content">
        <div class="container">
        <h2>Architects</h2>
        <p class="bold">Access CAD Files</p>
        <p>If you already have an account, click the button below to sign-in and access your CAD files.</p>
                <a href="http://resources.kstair.com/architects/">
                <div id="architect-btn">
            	<p>Click here to log-in</p>
                </div>
                </a>
        <p class="bold">Request Access to CAD Files</p>
        <p>If you do not have an account, please fill out the form below to request access. Your request will be reviewed within one business day and, upon approval, you'll receive an email with your approved username and password.</p>
        <p>Please note that only verified AIA architects at approved firms are approved. Call <strong>636-271-4002</strong> or email us at <strong><a href="mailto:cadfileaccessrequest@kstair.com?subject=Request Access to CAD Files">cadfileaccessrequest@kstair.com</a></strong> if you have questions.</p>
        <div id="request-cad-form"><?php gravity_form(2, false, false, false, false, false, false); ?></div>
        </div>
        </div>
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
