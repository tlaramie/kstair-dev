<?php /*Template Name: Warranty Registration*/?>

<?php get_header(); ?>

<div id="primary" class="content-area">

	<main id="main" class="site-main" role="main">
    
    	<div id="scrolling-content"></div>
        
        <div id="registration-content">
        
        	<div class="container">
            
            	<div id="registration-form">
                
                	<h2>Countertop Warranty Registration</h2>
                    
                    <p>Please fill out the form below to register your new countertop's warranty.</p>
                    
                    <div id="registration-form-container">
                    
                    	<?php gravity_form(4, false, false, false, false, false, false); ?>                    
                    
                    </div>
                
                </div>
                
                <div id="contact-info">
                    
                    <p><span class="bold">CORPORATE OFFICE</span><br/>50 Midwest Drive<br/>Pacific, MO 63069</p>
                    
                    <p>p: 636-271-4002<br/>f: 636-271-4098</p>
                    
                    <p><span class="bold">HOME GALLERY</span><br/>744 Hanley Industrial Court<br/>St. Louis, MO 63144</p>
                    
                    <p>p: 314-781-5151<br/>f: 314-781-5161</p>
                   
                    <div id="testimonials">
                    
                    	<?php
                    
                    		$args = array( 'posts_per_page' => 1, 'orderby' => 'rand', 'cat' => '9' );
                    
                    		$rand_posts = get_posts( $args );
                   
                   			 foreach ( $rand_posts as $post ) : 
                    
                    		setup_postdata( $post );
                            
                        ?>
                        
                        <?php the_content(); ?>
                    	
                        <?php endforeach; 
                   		
                         wp_reset_postdata(); 
                         
                         ?>
                         
                    </div>
        
        		</div>
            
            </div>
        
        </div>
       
    </main>
    
</div>

<?php get_footer(); ?>