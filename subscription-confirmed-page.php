<?php /*Template Name: Subscription Confirmed*/?>


<?php get_header(); ?>
		

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
		<div id="thank-you-content">
        <div class="title-image">
        <div class="container">
        <h2>Subscription Confirmed</h2>
        </div>
        </div>
        <div class="container">
        <?php  while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.  ?>
        </div>
        </div>
        
        
        <div class="call-out-bar">
        <div class="container">
        <p>Give us a call at 314.781.5151</p>
        </div>
        </div>
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
