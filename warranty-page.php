<?php /*Template Name: Warranty*/ ?>

<?php get_header(); ?>

<div id="primary" class="content-area">

	<main id="main" class="site-main" role="main">
    
    	<div id="scrolling-content" style="height: 50px;"></div>
        
        <div id="warranty-content">
        
        	<header class="warranty-header" style="background-image: url('<?php bloginfo('template_directory');?>/images/countertopA.jpg');">
        
        		<h2>Limited Lifetime Warranty</h2>
                
            </header>
            
            <section class="warranty-content">
            
                <p>KS&amp;M wood countertops have a limited lifetime warranty.</p>
                
                <p>We’ll repair or replace (at our discretion) any wood countertop due to manufacturing defects.</p>
                
                <p>Wood is a natural, living and breathing product.  We cannot be responsible for naturally occurring movement of wood due to seasonal changes, poor environmental conditions in the home such as high or low humidity, or poor maintenance of the wood top.</p>
                
                <p>We will make every effort to match color samples; however, because trees grow in different conditions and environments, and because color and grain patterns often vary due to these conditions, we cannot guarantee exact matches to a sample.</p>
                
                <p>We cannot warranty wood that may crack, warp, or split due to dryness or poor maintenance (please refer to <em>care and maintenance</em> of your wood countertop), any unfinished tops that are shipped, or for surface scratches in any finish or countertop.</p>
                
                <p>At time of packaging, we will photograph and include a picture of your wood countertop in the packing crate to verify its quality. Any claim of shipping damage must be made within five (5) working days of delivery.</p>
                
                <p><strong>Please Note:</strong> Registration is <em>required</em> in order for warranty to be valid.</p>
                
                <a href="/warranty-registration"><button class="newsletter-btn">Warranty Registration</button></a>
                
           </section>
        
        </div>
       
    </main>
    
</div>

<?php get_footer(); ?>