<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package KStair
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
       <div id="main-footer">
        <div id="top-page-button">
       <p><a href="#top">Top</a></p>
       </div>
       <div class="container">
       <div id="footer-logo">
       <a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-logo.svg" alt="Kirkwood Stair & Millwork"></a>
       </div>
       <div id="footer-social">
       <a href="https://www.facebook.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-facebook.svg" alt="Friend us on Facebook"></a>
       <a href="http://www.pinterest.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-pinterest.svg" alt="Follow us on Pinterest"></a>
       <a href="http://www.houzz.com/pro/kirkwoodstairandmillwork/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-houzz.svg" alt="Follow us on Houzz"></a>
       </div>
       <nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle"><?php _e( 'Primary Menu', 'kstair' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
            <p id="footer-phone">314.781.5151</p>
		</nav><!-- #site-navigation -->
		</div><!-- .container -->
        </div><!-- #main-footer -->
        <div class="site-info">
        	<p>&copy; Copyright <?php echo date('Y'); ?> &#124; 744 Hanley Industrial Ct, Brentwood, MO 63144</p>
			<!--<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'kstair' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'kstair' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( __( 'Theme: %1$s by %2$s.', 'kstair' ), 'KStair', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>-->
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<?php
	if(is_front_page()) {
?>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/homepage-carousel.js" async defer></script>
<?php
	}
?>

<script>
var previousScrollPosition = 0;
var lazySliderLoaded = false;
function lazyLoadSlider() {
	var dir = "<?php bloginfo('template_directory'); ?>";
	var slides = jQuery('.lazy-slider').data("slides");
	var html = "";
	for(var x = 0; x < slides.length; x++) {
		if(x == 0) {
			html += '<div class="item active">';	
		} else {
			html += '<div class="item">';	
		}
		html += '<picture><source srcset="' + dir + '/images/' + slides[x] + '-l-min.jpg" media="(min-width: 1201px)" /><source srcset="' + dir + '/images/' + slides[x] + '-m-min.jpg" media="(min-width: 601px)" /><source srcset="' + dir + '/images/' + slides[x] + '-s-min.jpg" media="(max-width: 600px)" /><img src="' + dir + '/images/' + slides[x] + '-l-min.jpg" /></picture>';
		html += '</div>';
	}
	jQuery('.lazy-slider').empty();
	jQuery('.lazy-slider').append(html);
	jQuery('.carousel').carousel({
		interval: 5000
	});
}
jQuery(function($) {
   $(document).ready(function(){
	   var navExcludedList = [186, 169, 114, 112, 107, 11, 203, 207, 212, 187, 50, 26, 21, 17, 228, "single", "archive", "error404"];
	   for(var x = 0; x < navExcludedList.length; x++) {
			if(isNaN(navExcludedList[x])) {
				var el = "." + navExcludedList[x] + " .navbar";
			} else {
				var el = ".page-id-" + navExcludedList[x] + " .navbar";
			}
			if($(el).length !== 0) {
				$(el).addClass("excluded fixed");
			}
	   }
	   $(window).bind('scroll', function() {
		   if($('.navbar.excluded').length === 0) {
				if($(window).scrollTop() > previousScrollPosition) {
					//Scroll Down
					var h = 90;
				} else {
					//Scroll Up
					var h = 40;
				}
				previousScrollPosition = $(window).scrollTop();
				var navHeight = $( window ).height() - h;
				 if ($(window).scrollTop() > navHeight) {
					 $('.navbar').addClass('fixed');
				 }
				 else {
					 $('.navbar').removeClass('fixed');
				 }
		   } else {
				$('.navbar.excluded').addClass("fixed");   
		   }
		   if($('.lazy-slider').length > 0 && !lazySliderLoaded) {
			   lazyLoadSlider();
			   lazySliderLoaded = true;
		   }
		});
		
		$('a[href*="#"]:not([href="#"])').not("a.carousel-control").click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  		var target = $(this.hash);
		  		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  		if (target.length) {
					$('html, body').animate({
			  			scrollTop: target.offset().top
					}, 1000);
					return false;
		  		}
			}
	    });
		
	});
});
</script>

</body>
</html>
