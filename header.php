<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package KStair
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!--FavIcon Info-->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="apple-mobile-web-app-title" content="Kirkwood Stair">
<meta name="application-name" content="Kirkwood Stair">
<meta name="theme-color" content="#ffffff">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WCX94Z');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager May 2021 Update-->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);<https://www.googletagmanager.com/gtm.js?id=%27+i+dl;f.parentNode.insertBefore(j,f);>
})(window,document,'script','dataLayer','GTM-5NQCRMG');</script>
<!-- End Google Tag Manager -->

<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>

<!--<link rel="stylesheet" href="<?/*php bloginfo('template_directory');*/ ?>/custom.css?3445">-->

<script type="text/javascript" src="//use.typekit.net/yjj4otv.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) 
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WCX94Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Google Tag Manager May 2021 Update (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NQCRMG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'kstair' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<!--<div class="site-branding">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		</div>-->

       <nav id="site-navigation" class="main-navigation navbar" role="navigation">
		<?php
    		$args1 = array( 'menu' => 'header_menu_1' );
    		$args2 = array( 'menu' => 'header_menu_2' );
    		wp_nav_menu($args1);
		?>
		<div id="nav-logo">
			<a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/kirkwood-stairs-logo.svg" alt="Kirkwood Stair &amp; Millwork"></a>
			<div id="mobile-menu-icon" onclick="$('body').toggleClass('menu-open')">
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>
		<?php
    		wp_nav_menu($args2);
		?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
