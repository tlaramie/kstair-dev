<?php /*Template Name: Home Page*/?>

<?php get_header(); ?>

<!-- Featured Background-->
		<div id="top"></div>
		<div class="fullscreen" id="home-feature">
            <div class="feature-text">
                <div class="feature-text-wrapper">
                    <div class="feature-text-logo">
                    	<img src="<?php bloginfo('template_directory'); ?>/images/kstair-logo.svg">
                    </div>
                    <p>We’ve crafted custom and architectural millwork since 1899. It’s our passion, and that shows in the quality, detail, and soul we put into every single woodwork project.</p>
                    <div class="scrolling-arrow">
                        <p class="center"><a href="#scrolling-content">Scroll Down</a></p>
                        <a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
                    </div>
                </div>
            </div>
            <div class="homepage-carousel-controls">
            	<div>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 251.84 251.84" class="wood-countertops-icon" onclick="changeSlide(0);"><defs><style>.wood-countertops-icon .a{fill:#c7b299;}.wood-countertops-icon .b{fill:#231f20;}</style></defs><title>Wood Countertops</title><circle class="a" cx="125.92" cy="125.92" r="125.92"/><path class="b" d="M182.46,83.2H71l-36.5,82.67H221.12ZM75.12,157.78l13.31-44.25h51.78l8.1,44.25Zm84-17.42h-5.77v-7h-5.24v-7h5.51v-26c0-3.3-3.54-6.47-8.59-6.47s-8.13,3.43-8.19,8.19l-4.6,0c0-2.63,1.59-14.41,12.67-14.41s14.22,7.13,14.22,11.53Z" transform="translate(-1.87 -2.03)"/><rect class="b" x="32.59" y="166.17" width="186.67" height="4.5"/></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 252.15 252.15" class="stairs-icon" onclick="changeSlide(1);"><defs><style>.stairs-icon .a{fill:#c7b299;}.stairs-icon .b{fill:#231f20;}</style></defs><title>Stairs</title><circle class="a" cx="126.07" cy="126.07" r="126.07"/><polygon class="b" points="79.97 65.87 75.86 72.43 174.74 72.43 171.51 65.87 79.97 65.87"/><rect class="b" x="75.86" y="73.5" width="98.88" height="14.38"/><polygon class="b" points="73.5 91.97 68.82 99.43 181.4 99.43 177.72 91.97 73.5 91.97"/><rect class="b" x="68.82" y="100.64" width="112.57" height="16.38"/><polygon class="b" points="65.8 121.2 60.41 129.8 190.19 129.8 185.96 121.2 65.8 121.2"/><rect class="b" x="60.41" y="131.3" width="129.79" height="18.78"/><polygon class="b" points="60.12 154.84 54.15 164.38 198 164.38 193.31 154.84 60.12 154.84"/><rect class="b" x="54.15" y="166.35" width="143.85" height="19.92"/></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 251.69 251.69" class="doors-windows-icon" onclick="changeSlide(2)"><defs><style>.doors-windows-icon .a{fill:#c7b299;}.doors-windows-icon .b{fill:#070808;}</style></defs><title>Doors and Windows</title><circle class="a" cx="125.85" cy="125.85" r="125.85"/><path class="b" d="M48.1,49.82v79.26h68.62V49.82Zm5.31,5.05H80.9v33H53.42ZM81,124.23H53.48v-33H81Zm2.86-69.46H111.3v33H83.82Zm27.54,69.36H83.88v-33h27.48Z" transform="translate(-2.17 -3)"/><path class="b" d="M126,51.68v156.2H210.7V51.68Zm11.34,85.81a3.85,3.85,0,1,1,3.85-3.85A3.85,3.85,0,0,1,137.3,137.49Z" transform="translate(-2.17 -3)"/><rect class="b" x="43.17" y="127.71" width="74.16" height="5.23"/></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 252.12 252.12" class="millwork-icon" onclick="changeSlide(3);"><defs><style>.millwork-icon .a{fill:#c7b299;}.millwork-icon .b{fill:#231f20;}</style></defs><title>Millwork</title><circle class="a" cx="126.06" cy="126.06" r="126.06"/><path class="b" d="M38.65,195.69V61H79.81a41.22,41.22,0,0,1,2.09,4.5c9.7,30.06,22.17,40.08,54,42,15.75.94,30.16,4.61,39,18.58,5,7.84,7.57,17.15,11.76,27h30.89v42.63Z" transform="translate(-2.03 -2.27)"/></svg>
                    <div style="clear:both;"></div>
                </div>
                <div class="homepage-carousel-text">
                	<div>
                    	<p><a href="#home-A">Wood Countertops <img src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg" alt="Scroll Down Arrow" /></a></p>
                 	</div>
                  	<div>
                    	<p><a href="#home-B">Stairs <img src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg" alt="Scroll Down Arrow" /></a></p>
                  	</div>
                 	<div>
                    	<p><a href="#home-D">Doors and Windows <img src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg" alt="Scroll Down Arrow" /></a></p>
                  	</div>
                 	<div>
                    	<p><a href="#home-C">Millwork <img src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg" alt="Scroll Down Arrow" /></a></p>
                 	</div>
           		</div>
            </div>
        </div>
        
        <div id="scrolling-content"></div>

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph><a href="tel:314-781-5151">314.781.5151</a></ph>
            <p class="italic">Contact us with any questions and comments.</p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an Estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
        <div id="home-A">
            <div class="container">
                <div class="home-page-text">
                    <h2>Wood Countertops</h2>
                    <p>Wood countertops deliver an incredible presence and balance to any space. Discover why so many designers are turning to the beauty and durability of natural wood countertops for kitchen, islands and bars. </p>
                    <p><a href="/wood-countertops">Click here for more information &gt;</a></p>
                    <hr/>
                    <a href="http://kstairquote.com/Quote/Step1" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/estimator-logo-white.svg" alt="Kirkwood Stair Quoting Engine"></a>
                    <br/><br/>
                    <p class="italic">Get an estimate for your custom wood countertop.</p>
                </div>
            </div>
        </div>

        <div id="home-B">
            <div class="container">
                <div class="home-page-text">
                    <h2>Stairs</h2>
                    <p>Stairs make a strong architectural statement. Find out how our team of craftsmen and professionals can creatively solve any structural or design challenges from initial design to the finished installation.</p>
                    <p><a href="/stairs">Click here for more information &gt;</a></p>
                    <hr/>
                    <a href="/contact/"><img src="<?php bloginfo('template_directory'); ?>/images/stairs-estimate.svg" alt="Get an estimate on custom stairs"></a>
                    <br/><br/>
                    <p class="italic">Get an estimate for your stairs.</p>
                </div>
            </div>
        </div>
        
        <div id="home-D">
            <div class="container">
                <div class="home-page-text">
                    <h2>Doors and Windows</h2>
                    <p>We'll customize windows and doors according to your home's unique style and space. Find out how our 60 years of experience in the window and door industry assures that your project will exceed your expectations.</p>
                    <p><a href="/doors-windows">Click here for more information &gt;</a></p>
                    <hr/>
                    <a href="/contact/"><img src="<?php bloginfo('template_directory'); ?>/images/doors-windows-estimate.svg" alt="Get an estimate on custom doors and windows work"></a>
                    <br/><br/>
                    <p class="italic">Get an estimate for your windows and doors.</p>
                </div>
            </div>
        </div>
        
        <div id="home-C">
            <div class="container">
                <div class="home-page-text">
                    <h2>Custom Millwork</h2>
                    <p>From arched casings to crown moulding, bookcases to library paneling, hand-hewn beams to mantles, we've got your custom millwork needs covered. Our collaborative approach ensures that your ideas reach their full potential.</p>
                    <p><a href="/mouldings">Click here for more information &gt;</a></p>
                    <hr/>
                    <a href="/contact/"><img src="<?php bloginfo('template_directory'); ?>/images/custom-millwork-estimate.svg" alt="Get an estimate on custom millwork"></a>
                    <br/><br/>
                    <p class="italic">Get an estimate for your custom millwork.</p>
                </div>
            </div>
        </div>
        
        <div id="home-E">
        	<div class="container">
            	<div class="home-page-text">
                	<h2>We Put Our Best Woodwork Forward</h2>
                    <p>Kirkwood Stair &amp; Millwork has a deep, rich history of designers, craftsmen, wood carvers, and installers who create handcrafted, custom millwork - from stunning stairs to sleek wood countertops -- to your precise specifications. Our builder, contractor, homeowner and designer clients rely on us to make their woodwork concepts and plans come to life. Our company has a cool culture - one that inspires each of us to do our best work with precision, timeliness and artfulness in our effort to always exceed expectations. One of our greatest rewards, truly, is to see how happy we’ve made our clients.</p>
                </div>
            </div>
        </div>
        
        <div id="home-F">
        	<div class="container">
                <div class="half-container">
                    <div>
                        <a href="/our-work"><img src="<?php bloginfo('template_directory'); ?>/images/big-cedar-callout.png" alt="A close up of the construction of the Big Cedar Staircase" /></a>
                    </div>
                    <div>
                        <h2 class="italic bold"><a href="/our-work">Big Cedar</a></h2>
                        <p><a href="/our-work">Check out how we constructed and installed the grand staircase in the Big Cedar Lodge in Branson, MO <strong>&gt;</strong></a></p>
                    </div>
                </div>
                <div class="half-container">
                    <div>
                        <a href="/our-work"><img src="<?php bloginfo('template_directory'); ?>/images/design-center-callout.png" alt="A piece of wood describing the Design Center" /></a>
                    </div>
                    <div>
                        <h2 class="italic bold"><a href="">Design Center</a></h2>
                        <p><a href="">During the design and concept phase, our sales staff, designers, and support staff work with your builder, architect, and designer to translate your ideas from paper possibilities into digital realities <strong>&gt;</strong></a></p>
                    </div>
                </div>
        	</div>
        </div>
        
        <!--<div id="home-C">
            <div class="container">
                <div class="home-page-text">
                    <h2>We Put Our Best Woodwork Forward</h2>
                    <p>Our builder, contractor, homeowner and designer clients rely on us to make their woodwork concepts and plans come to life. Our company has a cool culture - one that inspires each of us to do our best work with precision, timeliness and artfulness in our effort to always exceed expectations. One of our greatest rewards, truly, is to see how happy we’ve made our clients.</p>
                </div>
            </div>
        </div>
        
        <div id="home-D">
            <div class="container">
                <div class="home-page-text">
                    <h2 class="bold">Our Design Center</h2>
                    <p>Ours is a one-stop design center, a cutting-edge concept center that's capable of producing a range of looks from traditional to contemporary. During the design and concept phase, our sales staff, designers and support staff work with your builder, architect, and designer to translate your ideas from paper possibilities into digital realities.</p>
                    <p class="italic">This collaborative approach helps ensure your design is expertly cared for and understood by craftsmen who build your countertops, stairs, millwork, doors and windows.</p>
                </div>
            </div>
        </div>        
        <div id="home-E">
        <div class="container">
        <a href="/our-work"><img src="<?php bloginfo('template_directory'); ?>/images/big-cedar-callout.png"></a>
        <div class="home-page-text">
        <h2 class="italic bold"><a href="/our-work">Big Cedar</a></h2>
        <p class="bold"><a href="/our-work">Check out how we constructed and installed the grand staircase in the Big Cedar Lodge in Branson, MO &gt;</a></p>
        </div>
        </div>
        </div>        
        <div id="home-F">
        <div class="container">
        <div class="home-page-text">
        <h2>Craftsmanship</h2>
        <p>Once the design is created, we pursue the best domestic hard woods (with an eye towards sustainability), like Red and White Oak, Poplar, Steamed Walnut and Cherry as well as exotics such as Mahogany, Jatoba, Tigerwood, and even Bamboo. We purchase all of our lumber from mills practicing sustainable and environmental responsibility.</p>
        <p>When the raw lumber arrives, our craftsmen blend old world joineries like mortise and tenon, and dove- tailing with the most advanced woodworking equipment in the world to create unique, architectural features. We create all of our countertops, stairs, millwork, and doors at our plant in Pacific, Missouri. </p>
        </div>
        </div>
        </div>        
        
        <div class="call-out-bar">
        <p><a href="/contact">Questions? Comments? Need a quote? Click here to contact us.</a></p>
        </div>-->
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
