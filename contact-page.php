<?php /*Template Name: Contact*/?>


<?php get_header(); ?>


		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        
        <div id="contact-content">
        <div class="container">
        <div id="contact-form">
        <h2>We Wood Love to Hear From You</h2>
        <p>If you have questions or would like to talk with one of our representatives, please call us at <br/><br/><span class="bold">314-781-5151</span></p>
        <p>or complete the form below and we'll contact you.</p>
       <div id="request-cad-form"><iframe height="1100" allowTransparency="true" frameborder="0" scrolling="no" style="width:100%;border:none"  src="https://dzinewise.wufoo.com/embed/r169u7eu1jakmed/"><a href="https://dzinewise.wufoo.com/forms/r169u7eu1jakmed/">Fill out my Wufoo form!</a></iframe></div>
        </div>
        <div id="contact-info">
        <a href="http://kstairquote.com/Quote/Step1" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/estimator-logo.svg" alt="Kirkwood Stair Quoting Engine"></a>
        <br/><br/>
        <p class="italic">Use this application to get an estimate for your custom wood countertop.</p>
        <hr/>
        <p>744 Hanley Industrial Court<br/>St. Louis, MO 63144</p>
        <p>p: 314-781-5151<br/>f: 314-781-5161</p>
       	<hr/>
        <div class="contact-resource-callout">
        	<h2>Access CAD Files</h2>
            <p class="about">If you already have an account, log in to access your CAD files; otherwise, follow the request access link below.</p>
            <p><a href="http://resources.kstair.com/architects/">Log In</a></p>
            <p><a href="/resources/">Request Access</a></p>
        </div>
        <hr/>
        <div id="testimonials">
		<?php
		$args = array( 'posts_per_page' => 1, 'orderby' => 'rand', 'cat' => '9' );
		$rand_posts = get_posts( $args );
		foreach ( $rand_posts as $post ) : 
  		setup_postdata( $post ); ?>
			<?php the_content(); ?>
		<?php endforeach; 
		wp_reset_postdata(); ?>
        </div>
        
        </div>
        </div>
        </div>
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
