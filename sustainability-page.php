<?php /*Template Name: Sustainability*/?>


<?php get_header(); ?>
		

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>314.781.5151</ph>
            <p class="italic">Contact us with any questions and comments. </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
        <div id="sustainability-content">
        <div id="sustainability-content-wrapper">
        <div id="sustainability-content-inside-wrapper">
        <h2 class="italic">Sustainability</h2>
        <p>We believe in responsible practices and procedures to minimize impact on the environment. We’re committed to operating a business based on the principles of sustainability.</p>
        <p>We purchase all of our lumber from mills practicing sustainable and environmental responsibility.</p>
        <p>We believe in maximizing wood materials, including using scrap pieces for smaller finishing profiles or other products. We keep scrap wood out of landfills by grinding and blending it with our shavings for use in area cattle and horse farms, and then spread it in adjacent farm fields.  </p>
        </div>
        </div>
        <div class="trees">&nbsp;</div> 
        </div>
        <div id="sustainability-green">
        <div class="container">
        <img src="<?php bloginfo('template_directory'); ?>/images/globe.png" alt="Renewable & Biodegradable">
        <p>As a member of  <a href="http://www.univerdant.com/" target="_blank">Univerdant</a>, a network for implementing global sustainable solutions, Kirkwood Stair &amp; Millwork uses a team approach toward sustainability and green solutions. We are currently working on our Forest Stewardship Council<sup>&trade;</sup> FSC Certification to improve our qualifications on LEED projects. </p>
        </div>    
        </div>
        
        
        <div class="call-out-bar">
        <p>We are proud to create exceptional quality, craftsmanship and value while sustaining our vital natural resources.</p>
        </div>
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
