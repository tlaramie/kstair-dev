// JavaScript Document
var homepageCarouselInterval = 8000;
var homepageCarouselTransition = 1000;
var homepageSlideInProgress = false;
var homepageSlideController, homepageCurrentSlide;
function getTemplateDirectory() {
	var imgSrc = jQuery('#home-feature').find("img").attr("src");
		imgSrc = imgSrc.substring(0, imgSrc.indexOf("images") - 1);
	return imgSrc;
}
function getImageList() {
	return [
		{
			"id": "0",
			"text": "Wood Countertops",
			"class": "homepage-slide-wood-countertops"	
		},
		{
			"id": "1",
			"text": "Stairs",
			"class": "homepage-slide-stairs"
		},
		{
			"id": "2",
			"text": "Windows &amp; Doors",
			"class": "homepage-slide-windows-doors"
		},
		{
			"id": "3",
			"text": "Custom Millwork",
			"class": "homepage-slide-custom-millwork"
		}
	];	
}
function randomStart(random) {
	jQuery('.homepage-carousel-controls svg:eq(' + random + ')').addClass("active");
	jQuery('.homepage-carousel-text div:eq(' + random + ')').addClass("active");
	if(random != 0) {
		for(var x = 0; x < random; x++) {
			jQuery('#home-feature .slide:eq(' + x + ')').css("opacity", "0");
		}
	}
}
function initializeSlider(images, callback) {
	var home = jQuery('#home-feature');
	var zIndex = 0;
	var templateDirectory = getTemplateDirectory();
	for(var x = 0; x < images.length; x++) {
		zIndex--;
		var html = "<div class=\"slide " + images[x].class + "\" style=\"z-index: " + zIndex + "\"></div>";
		home.append(html);
	}
	home.css({ "background-color": "transparent", "background-image": "none" });
	var random = Math.floor((Math.random() * 4));
	randomStart(random);
	callback(random);
}
function slideController(x) {
	homepageCurrentSlide = x;
	homepageSlideController = setInterval(function() {
		rotateSlides();
	}, homepageCarouselInterval);
}
function changeSlide(slide) {
	if(!homepageSlideInProgress && !jQuery('.homepage-carousel-controls svg:eq(' + slide + ')').hasClass("active")) {
		homepageSlideInProgress = true;
		var images = getImageList();
		clearInterval(homepageSlideController);
		var zIndex = jQuery('#home-feature .slide:eq(' + slide + ')').css("z-index");
		jQuery('#home-feature .slide:eq(' + slide + ')').css("z-index", "-"+(images.length + 1)).css("opacity","1");
		if(slide == 0) {
			changeControl(images, (images.length - 1));		
		} else {
			changeControl(images, (slide - 1));	
		}
		jQuery('#home-feature .slide').not(":eq(" + slide + ")").animate({ "opacity": "0" }, homepageCarouselTransition);
		setTimeout(function() {
			jQuery('#home-feature .slide:eq(' + slide + ')').css("z-index", zIndex);
			for(var x = 0; x < images.length; x++) {
				if(jQuery('#home-feature .slide:eq(' + x + ')').css("z-index") > zIndex) {
					jQuery('#home-feature .slide:eq(' + x + ')').css("opacity", "1");
				}
			}
			slideController(slide);
			homepageSlideInProgress = false;	
		}, (homepageCarouselTransition + 250));
	}
}
function changeControl(images, x) {
	setTimeout(function() {
		jQuery('.homepage-carousel-controls svg.active').removeClass("active");
		jQuery('.homepage-carousel-text div.active').removeClass("active");
		if(x == (images.length - 1)) {
			jQuery('.homepage-carousel-controls svg:first-child').addClass("active");
			jQuery('.homepage-carousel-text div:first-child').addClass("active");
		} else {
			jQuery('.homepage-carousel-controls svg:eq(' + (x + 1) + ')').addClass("active");
			jQuery('.homepage-carousel-text div:eq(' + (x + 1) + ')').addClass("active");
		}
	}, (homepageCarouselTransition / 4));
}
function rotateSlides() {
	homepageSlideInProgress = true;
	var images = getImageList();
	if(homepageCurrentSlide == 0) {
		jQuery('#home-feature .slide:eq(0)').css("z-index", "-1");
		jQuery('#home-feature .slide').not(":eq(0)").css("opacity", "1");
	}
	if(homepageCurrentSlide == (images.length - 1)) {
		jQuery('#home-feature .slide:eq(0)').css("z-index", "-"+(images.length + 1)).css("opacity", "1");
	}
	var cur = jQuery('#home-feature .slide:eq(' + homepageCurrentSlide + ')');
	changeControl(images, homepageCurrentSlide);
	cur.animate({ "opacity": "0" }, homepageCarouselTransition, function() {
		homepageSlideInProgress = false;	
		setTimeout(function() {
			jQuery('#home-feature .slide:eq(0)').css("z-index", "-1");
		}, 250);
	});
	
	if(homepageCurrentSlide == (images.length - 1)) {
		homepageCurrentSlide = 0;
	} else {
		homepageCurrentSlide++;	
	}
	
}
jQuery(document).ready(function() {
	var images = getImageList();
	initializeSlider(images, function(random) {
		slideController(random);
	});
});