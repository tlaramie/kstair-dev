<?php /*Template Name: News*/?>


<?php get_header(); ?>
		

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>314.781.5151</ph>
            <p class="italic">Contact us with any questions and comments. </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
		<div id="news-content">
        <div class="title-image">
        <div class="container">
        <h2>Tongue &amp; Groove</h2>
        <h3>News &amp; Notes from Kirkwood Stair</h3>
        </div>
        </div>
        <div id="social-news">
        <div class="container">
       	<p>Follow KStair to see our latest projects and stay up-to-date:</p>
       <a href="https://www.facebook.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-facebook.svg" alt="Friend us on Facebook"></a>
       <a href="http://www.pinterest.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-pinterest.svg" alt="Follow us on Pinterest"></a>
       <a href="http://www.houzz.com/pro/kirkwoodstairandmillwork/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-houzz.svg" alt="Follow us on Houzz"></a>
        </div>
        </div>
        <div class="container">
        <br/><br/>
                 
	 	<?php query_posts('cat=8&amp;showposts='.get_option('posts_per_page')); ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
        
		
		<?php
		$categories = get_the_category();
		$excluded_cat = '8';
		$separator = ' / ';
		$output = '';
		if($categories){
			foreach($categories as $category) {
				if (
					$category->cat_ID != $excluded_cat
				)
					$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
			}
			echo trim($output, $separator);
		}
		?>

			<h2><a href="<?php the_permalink() ?>"> <?php the_title(); ?></a></h2>

			<?php include (TEMPLATEPATH . '/_/inc/meta.php' ); ?>

			<div class="entry">
            <?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  			the_post_thumbnail();
			}
			?>
            <?php the_excerpt( $more_link_text , $strip_teaser ); ?> 
			</div>	
		
		</article>

	<?php endwhile; ?>

	<?php include (TEMPLATEPATH . '/_/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>

        </div>
        </div>
        
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
