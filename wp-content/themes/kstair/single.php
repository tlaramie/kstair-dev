<?php
/**
 * The template for displaying all single posts.
 *
 * @package KStair
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>314.781.5151</ph>
            <p class="italic">Contact us with any questions and comments. </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
        <div id="social-news">
        <div class="container">
       	<p>Follow KStair to see our latest projects and stay up-to-date:</p>
       <a href="https://www.facebook.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-facebook.svg" alt="Friend us on Facebook"></a>
       <a href="http://www.pinterest.com/Kstrair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-pinterest.svg" alt="Follow us on Pinterest"></a>
       <a href="http://www.houzz.com/pro/kirkwoodstair/kirkwood-stair-and-millwork" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-houzz.svg" alt="Follow us on Houzz"></a>
        </div>
        </div>
        
        
        <div class="blog-post container">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

			<?php kstair_post_nav(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				//if ( comments_open() || '0' != get_comments_number() ) :
					//comments_template();
				//endif;
			?>

		<?php endwhile; // end of the loop. ?>
		</div><!-- end of container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>