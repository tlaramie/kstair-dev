<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package KStair
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        
        <div id="scrolling-content"></div>
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>636.271.4002</ph>
            <p class="italic">Contact us with any questions and comments. </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request a Quote</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
        <div id="social-news">
        <div class="container">
       	<p>Follow KStair to see our latest projects and stay up-to-date:</p>
       <a href="https://www.facebook.com/pages/Kirkwood-Stair-Millwork/311962922882" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-facebook.svg" alt="Friend us on Facebook"></a>
       <a href="http://www.pinterest.com/Kstrair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-pinterest.svg" alt="Follow us on Pinterest"></a>
       <a href="http://www.houzz.com/pro/kirkwoodstair/kirkwood-stair-and-millwork" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-houzz.svg" alt="Follow us on Houzz"></a>
        </div>
        </div>
        
		<div class="container">
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
						if ( is_category() ) :
							single_cat_title();

						elseif ( is_tag() ) :
							single_tag_title();

						elseif ( is_author() ) :
							printf( __( 'Author: %s', 'kstair' ), '<span class="vcard">' . get_the_author() . '</span>' );

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'kstair' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'kstair' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'kstair' ) ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'kstair' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'kstair' ) ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'kstair' );

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							_e( 'Galleries', 'kstair' );

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'kstair' );

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'kstair' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'kstair' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'kstair' );

						elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
							_e( 'Statuses', 'kstair' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							_e( 'Audios', 'kstair' );

						elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
							_e( 'Chats', 'kstair' );

						else :
							_e( 'Archives', 'kstair' );

						endif;
					?>
				</h1>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php kstair_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
		</div><!-- end of container -->

		</main><!-- #main -->
	</section><!-- #primary -->
    
    <article>
    <a href="/news"><div class="back-btn">
    <p>GO BACK TO NEWS</p>
    </div></a>
    </article>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
