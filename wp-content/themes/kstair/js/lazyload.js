/* lazyload.js (c) Lorenzo Giuliani
 * MIT License (http://www.opensource.org/licenses/mit-license.html)
 *
 * expects a list of:  
 * `<img src="blank.gif" data-src="my_image.png" width="600" height="400" class="lazy">`
 */

!function(window){
  var $q = function(q, res){
        if (document.querySelectorAll) {
          res = document.querySelectorAll(q);
        } else {
          var d=document
            , a=d.styleSheets[0] || d.createStyleSheet();
          a.addRule(q,'f:b');
          for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
            l[b].currentStyle.f && c.push(l[b]);

          a.removeRule(0);
          res = c;
        }
        return res;
      }
    , addEventListener = function(evt, fn){
        window.addEventListener
          ? this.addEventListener(evt, fn, false)
          : (window.attachEvent)
            ? this.attachEvent('on' + evt, fn)
            : this['on' + evt] = fn;
      }
    , _has = function(obj, key) {
        return Object.prototype.hasOwnProperty.call(obj, key);
      }
    ;

  function styleDiv(div, par, img) {
    var backgroundPosition = par.getPropertyValue('background-position');
    var backgroundRepeat = par.getPropertyValue('background-repeat');
    var backgroundSize = par.getPropertyValue('background-size');
    var styles = 'position:absolute;top:0;left:0;right:0;bottom:0;z-index:-9999;background-image:url("'+img.src+'");background-position:'+backgroundPosition+';background-repeat:'+backgroundRepeat+';background-size:'+backgroundSize+';';
    div.className = 'lazyloadbg';
    div.style.cssText = styles;
  }

  function loadImage (el, fn) {
    var img = new Image()
      , src = el.getAttribute('data-src');
    img.onload = function() {
      if (!! el.parent)
        el.parent.replaceChild(img, el)
      else
        el.src = src;

      fn? fn() : null;
    }
    img.src = src;
  }

  function bgIsLoaded(el) {
    for( var i = 0; i < el.childNodes.length; i++) {
      if(el.childNodes[i].className == 'lazyloadbg'){
        return true;
      }
    }
    return false;
  }

  function loadBgImage (el, fn) {
    var img = new Image()
      , src = el.getAttribute('data-src')
      , hideBehind = el.getAttribute('data-hideBehind') !== null ? el.getAttribute('data-hideBehind') : false;
    img.onload = function() {
      if(hideBehind == 'true') {
        var style = window.getComputedStyle(el);
        var position = style.getPropertyValue('position');
        if(!bgIsLoaded(el)) {
          if(position != 'relative' || position != 'absolute' || position != 'fixed') {
            el.style.position = 'relative';
          }
          var div = document.createElement('div');
          styleDiv(div, style, img);
          el.appendChild(div);
          setTimeout(function() {
            el.style.backgroundImage = "none";
          }, 50);
        }
      } else {
        el.style.backgroundImage = 'url("' + img.src + '")';
      }
      fn? fn() : null;
    }
    img.src = src;
  }

  function elementInViewport(el) {
    var rect = el.getBoundingClientRect()

    return (
       rect.top    >= 0
    && rect.left   >= 0
    && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
    )
  }

    var images = new Array()
      , query = $q('img.lazy')
      , bgimages = new Array()
      , bgquery = $q('div.lazy')
      , processScroll = function(){
          for (var i = 0; i < images.length; i++) {
            if (elementInViewport(images[i])) {
              loadImage(images[i], function () {
                images.splice(i, i);
              });
            }
          };
          for(var x = 0; x < bgimages.length; x++) {
            if(elementInViewport(bgimages[x])) {
              loadBgImage(bgimages[x], function() {
                bgimages.splice(x, x);
              });
            }
          };
        }
      , processScrollMobile = function(){
          for(var i = 0; i < images.length; i++) {
            loadImage(images[i], function() {
              images.splice(i,i);
            });
          };
        }
      ;
    // Array.prototype.slice.call is not callable under our lovely IE8 
    for (var i = 0; i < query.length; i++) {
      images.push(query[i]);
    };
    for (var x = 0; x < bgquery.length; x++) {
      bgimages.push(bgquery[x]);
    };

    processScroll();
    addEventListener('scroll',processScroll);
    addEventListener('touchmove', processScrollMobile);

}(this);