<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package KStair
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
                    
        <div id="scrolling-content"></div>            
        
        		<section class="error-404 not-found container">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'kstair' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Choose from one of the links below or contact us for more information.', 'kstair' ); ?></p>
                  <br/><br/>
                  
                                                      
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>636.271.4002</ph>
            <p class="italic">Contact us with any questions and comments. </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request a Quote</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
                  
          <br/><br/> <br/><br/>        
                   
      <div class="clear">
      <div class="icon-callouts">
      <img src="<?php bloginfo('template_directory'); ?>/images/stairs-our-work.png">
      </div>
      <div class="intro-text">
      <h2>Custom Stairs</h2>
      <p>No other interior architectural element makes a stronger statement than your staircase. Learn more about stair planning and design. </p>
                <a href="/stairs">
                <div class="work-btn">
            	<p>Learn More</p>
                </div>
                </a>
      </div>
      </div>
      <br/>
      <div class="clear">
      <div class="icon-callouts right">
      <img src="<?php bloginfo('template_directory'); ?>/images/countertops-our-work.png">
      </div>
      <div class="intro-text">
      <h2>Custom Wood Countertops</h2>
      <p>Wood countertops bring incredible presence and balance to any space. Discover all of the advantages of choosing a wood countertop.</p>
                <a href="/wood-countertops">
                <div class="work-btn">
            	<p>Learn More</p>
                </div>
                </a>
      </div>
      </div>
      
      </div>
      </div>
      <br/><br/>              
                    
                    
                    

					<!--<?php get_search_form(); ?>-->

					<!--<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>-->

					<!--<?php if ( kstair_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
					<div class="widget widget_categories">
						<h2 class="widget-title"><?php _e( 'Most Used Categories', 'kstair' ); ?></h2>
						<ul>
						<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
					</div><!-- .widget -->
					<!--<?php endif; ?>

					<?php
						/* translators: %1$s: smiley */
						$archive_content = '<p>' . sprintf( __( 'Try looking in the monthly archives. %1$s', 'kstair' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
					?>

					<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>-->

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
