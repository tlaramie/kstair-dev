<?php /*Template Name: Vendor Partners*/?>


<?php get_header(); ?>
        

        <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
            <div class="newsletter-ad">
            <p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
            <button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
            <div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
            </div>
            <div class="contact-ad">
            <ph>314.781.5151</ph>
            <p class="italic">Contact us with any questions and comments. </p>
                <a href="/contact">
                <div class="contact-btn">
                <p>Request an estimate</p>
                </div>
                </a>
            </div>
        </div>
        </div><!-- end of .action bar -->

        <div id="vendor-content">
        <div class="title-image">
        <div class="container">
        <h2>Vendor Partners</h2>
        </div>
        </div>
        <div class="container">
        <p>Kirkwood Stair &amp; Millwork is dedicated to quality work, products and materials, and to completing your project on-time. We also set forth specific guidelines when it comes to the standards and integrity of our vendor partners. Our vendor partners excel in their specific fields. We rely upon their performance to match our standards and your expectations.</p>
        <h3>Moulding &amp; Milwork</h3>
        <div class="partner">
        <img src="<?php bloginfo('template_directory'); ?>/images/whiteriver-logo.jpg" alt="White River">
        <p>White River<br/><a href="http://www.whiteriver.com" target="_blank">www.whiteriver.com</a></p>
        </div>
        <div class="partner">
        <img src="<?php bloginfo('template_directory'); ?>/images/jpweaver-logo.jpg" alt="JP Weaver Company">
        <p>JP Weaver Company<br/><a href="http://www.jpweaver.com" target="_blank">www.jpweaver.com</a></p>
        </div>
        <div class="partner">
        <img src="<?php bloginfo('template_directory'); ?>/images/enkeboll-logo.jpg" alt="Enkeboll">
        <p>Enkeboll<br/><a href="http://www.enkeboll.com" target="_blank">www.enkeboll.com</a></p>
        </div>
        <h3>Stairs</h3>
        <div class="partner">
        <img src="<?php bloginfo('template_directory'); ?>/images/houseofforgings-logo.jpg" alt="House of Forgings">
        <p>House of Forgings<br/><a href="https://houseofforgings.net/" target="_blank">houseofforgings.net</a></p>
        </div>
        <div class="partner">
        <img src="<?php bloginfo('template_directory'); ?>/images/wmcoffman-logo.jpg" alt="WM Coffman">
        <p>WM Coffman<br/><a href="https://www.wm-coffman.com/" target="_blank">www.wm-coffman.com</a></p>
        </div>
        <div class="partner">
        <img src="<?php bloginfo('template_directory'); ?>/images/oakpointe-logo.jpg" alt="Oak Pointe">
        <p>Oak Pointe<br/><a href="http://www.stairpartsandmore.com" target="_blank">www.stairpartsandmore.com</a></p>
        </div>
        <div class="partner">
        <img src="<?php bloginfo('template_directory'); ?>/images/eurekaforge-logo.jpg" alt="Eureka Forge">
        <p>Eureka Forge<br/><a href="http://www.eurekaforge.com" target="_blank">www.eurekaforge.com</a></p>
        </div>
        </div>
        </div>
        
       
       <?php /* while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'content', 'page' );  ?>

        <?php endwhile; // end of the loop.*/  ?>
        </main><!-- #main -->
    </div><!-- #primary -->
    

<?php get_footer(); ?>
