<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package KStair
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
       <div id="main-footer">
        <div id="top-page-button">
       <p><a href="#top">Top</a></p>
       </div>
       <div class="container">
       <div id="footer-logo">
       	<div>
       	<a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-logo-homegal.svg" alt="Kirkwood Stair Home Gallery"></a>
       <p><strong>Windows &amp; Doors</strong><br/>744 Hanley Industrial Court<br/>St. Louis, MO 63144<br/><span id="footer-phone">p: 314.781.5151</span></p>
   		</div>
   		<div>
       <a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-logo.svg" alt="Kirkwood Stair & Millwork"></a>
       <p><strong>All Other Inquires</strong><br/>50 Midwest Drive<br/>Pacific, MO 63069<br/><span id="footer-phone">p: 636.271.4002</span></p>
   		</div>
       </div>
       <div id="footer-social">
       <a href="https://www.facebook.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-facebook.svg" alt="Friend us on Facebook"></a>
       <a href="http://www.pinterest.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-pinterest.svg" alt="Follow us on Pinterest"></a>
       <a href="http://www.houzz.com/pro/kirkwoodstairandmillwork/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kstair-houzz.svg" alt="Follow us on Houzz"></a>
       </div>
       <nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle"><?php _e( 'Primary Menu', 'kstair' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav><!-- #site-navigation -->
		</div><!-- .container -->
		<p id="footer-consultation-note">Stair consultations by appointment only</p>
        </div><!-- #main-footer -->
        <div class="site-info">
        	<p>&copy; Copyright <?php echo date('Y'); ?></p>
			<!--<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'kstair' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'kstair' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( __( 'Theme: %1$s by %2$s.', 'kstair' ), 'KStair', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>-->
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	<div id="mobile-menu">
		<?php
			$args = array("menu" => "mobile-menu");
			wp_nav_menu($args);
		?>
	</div>
	<div id="mobile-menu-backdrop"></div>
</div><!-- #page -->

<?php wp_footer(); ?>

<?php
	if(is_front_page()) {
?>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/homepage-carousel.js" async defer></script>
<?php
	}
?>

<script>
var previousScrollPosition = 0;
var lazySliderLoaded = false;
function lazyLoadSlider() {
	var dir = "<?php bloginfo('template_directory'); ?>";
	var slides = jQuery('.lazy-slider').data("slides");
	var html = "";
	for(var x = 0; x < slides.length; x++) {
		if(x == 0) {
			html += '<div class="item active">';	
		} else {
			html += '<div class="item">';	
		}
		html += '<picture><source srcset="' + dir + '/images/' + slides[x] + '-l-min.jpg" media="(min-width: 1201px)" /><source srcset="' + dir + '/images/' + slides[x] + '-m-min.jpg" media="(min-width: 601px)" /><source srcset="' + dir + '/images/' + slides[x] + '-s-min.jpg" media="(max-width: 600px)" /><img src="' + dir + '/images/' + slides[x] + '-l-min.jpg" /></picture>';
		html += '</div>';
	}
	jQuery('.lazy-slider').empty();
	jQuery('.lazy-slider').append(html);
	jQuery('.carousel').carousel({
		interval: 5000
	});
}
var StaggerEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();
jQuery(function($) {
	var navExcludedList = [186, 169, 114, 112, 107, 11, 203, 207, 212, 187, 50, 26, 21, 17, 228, 238, 230, "single", "archive", "error404", "parent-pageid-274"];
	for(var x = 0; x < navExcludedList.length; x++) {
		if(isNaN(navExcludedList[x])) {
			var el = "." + navExcludedList[x] + " .navbar";
		} else {
			var el = ".page-id-" + navExcludedList[x] + " .navbar";
		}
		if($(el).length !== 0) {
			$(el).addClass("excluded fixed");
		}
	}
	$(document).on('scroll', function() {
	    if($('.navbar.excluded').length === 0) {
			if($(window).scrollTop() > previousScrollPosition) {
				//Scroll Down
				var h = 90;
			} else {
				//Scroll Up
				var h = 40;
			}
			previousScrollPosition = $(window).scrollTop();
			var navHeight = $( window ).height() - h;
			 if ($(window).scrollTop() > navHeight) {
				 $('.navbar').addClass('fixed');
			 }
			 else {
				 $('.navbar').removeClass('fixed');
			 }
	    } else {
			$('.navbar.excluded').addClass("fixed");   
	    }
	    if($('.lazy-slider').length > 0 && !lazySliderLoaded) {
		   lazyLoadSlider();
		   lazySliderLoaded = true;
	    }
	});
	$('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(event) {
		if(location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		  if (target.length) {
			event.preventDefault();
			$('html, body').animate({
			  scrollTop: target.offset().top
			}, 1000);
		  }
		}
	});
	$('.doors-windows-types .icon').on("click", function() {
		var type = $(this).data("type");
		if(!$(this).hasClass("active")) {
			$('.icon.active').removeClass("active");
			$(this).addClass("active");
			if(type == "window") {
				$('#window-door-carousel').carousel("prev");	
			} else {
				$('#window-door-carousel').carousel("next");	
			}
		}
	});
	var GalleryControlInterval;
	$('.gallery-control')
		.on("mouseenter", function() {
			var $sib = $(this).siblings(".featured-gallery");
			var inc = $(this).hasClass("next") ? 2 : -2;
			GalleryControlInterval = setInterval(function(){
				var cur = $sib.scrollLeft() + inc;
				$sib.scrollLeft(cur);
			}, 1);
		})
		.on("mouseleave", function() {
			clearInterval(GalleryControlInterval); 
		});
	$('.featured-gallery').on("scroll", function() {
	   var $el = $(this);
	   StaggerEvent(function() {
		  var scroll = $el.scrollLeft();
		  var w = $el.find("div:first-child").innerWidth() - $el.innerWidth();
		  if(scroll > 0) {
			  $el.siblings(".gallery-control.prev").addClass("active");
		  } else {
			  $el.siblings(".gallery-control.prev").removeClass("active");
		  }
		  if(scroll === w) {
			  $el.siblings(".gallery-control.next").removeClass("active");
		  } else {
			  $el.siblings(".gallery-control.next").addClass("active");
		  }
	   }, 250, "WindowResizeEvent");
	});
});
</script>

</body>
</html>
