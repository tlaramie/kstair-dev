<?php /*Template Name: Window or Door Type Template*/?>


<?php get_header(); ?>


		<div id="primary" class="content-area window-door-type">
		<main id="main" class="site-main" role="main">
        <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
          
		  <div class="header-wrap" style="background: linear-gradient(rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35)), url('<?php echo $backgroundImg[0]; ?>') no-repeat; background-size:cover !important; background-position: center center;">
		     <header class="entry-header">
		    	<h1 class="entry-title title-keep featured-title italic"><?php the_title(); ?></h1>
		     </header>
		  </div> 
        
	        <div class="container" style="margin-top:2em;">
			   <?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'notitle' );  ?>

				<?php endwhile; // end of the loop.  ?>
	        </div>
	        <div class="call-out-bar">
                <p><a href="/contact/"><strong>Click here</strong> to schedule a consultation.</a></p>
            </div>
		</main><!-- #main -->
	</div><!-- #primary -->
 
 

<?php get_footer(); ?>
