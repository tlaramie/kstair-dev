 <?php /*Template Name: Our Work*/?>

<?php get_header(); ?>

<!-- Featured Background-->
        <div id="top"></div>
        <div class="fullscreen" id="work-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <h1 class="featured-title italic">Our Work</h1>
        <p>Find inspiration in a few of our recent projects.</p>
        <div class="scrolling-arrow">
            <p class="center"><a href="#scrolling-content">Scroll Down</a></p>
            <a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
        </div>
        </div>
        </div>
        </div>
        
        <div id="scrolling-content"></div>

        <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
            <div class="multiple-phone">
                <div class="phone">
                    <p>Windows &amp; Doors <br/> <a href="tel:314.781.5151">314.781.5151</a></p>
                </div>
                <div class="phone">
                    <p>All other inquiries <br/> <a href="tel:636.271.4002">636.271.4002</a></p>
                </div>
            </div>
            <div class="container">
                <div class="newsletter-ad">
                    <p class="subscribe">Subscribe</p>
                    <p class="italic">Stay in touch and receive exclusive offers.</p>
                    <button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
                    <div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
                </div>
                <div class="contact-ad">
                    <p class="subscribe">Contact</p>
                    <p class="italic">Contact us with any questions and comments.</p>
                    <a href="/contact">
                        <div class="contact-btn">
                            <p>Request an Estimate</p>
                        </div>
                    </a>
                </div>
            </div>
        </div><!-- end of .action bar -->
        
      <!-- Our Work Previews -->
      <div id="our-work">
      <div id="our-work-craftsmanship">
        <div class="container">
            <div class="home-page-text">
                <h2>Craftsmanship</h2>
                <p>Once the design is created, we pursue the best domestic hard woods (with an eye towards sustainability), like Red and White Oak, Poplar, Steamed Walnut and Cherry as well as exotics such as Mahogany, Jatoba, Tigerwood, and even Bamboo. We purchase all of our lumber from mills practicing sustainability environmental responsibility.</p>
                <p>When the raw lumber arrives, our craftsmen blend old world joineries like mortise and tenon, and dove-tailing with the most advanced woodworking equipment in the world to create unique, architectural features. We create all of our countertops, stairs, millwork, and doors at our plant in Pacific, Missouri. </p>
            </div>
        </div>
        <div style="clear:both;"></div>
      </div>
      <div class="container">
      
      <div class="clear">
      <div class="icon-callouts">
      <img src="https://kstair.com/wp-content/uploads/2024/02/stairs-our-work.webp" alt="Wooden Staircase and Stairs icon">
      </div>
      <div class="intro-text">
      <h2>Custom Stairs</h2>
      <p>No other interior architectural element makes a stronger statement than your staircase. See the story of how we created the <span class="bold">grand staircase for the Big Cedar Lodge in Branson, MO</span>. </p>
                <a href="custom-stairs">
                <div class="work-btn">
                <p>See Our Featured Project</p>
                </div>
                </a>
      </div>
      </div>
      <div class="clear">
      <div class="icon-callouts right">
      <img src="https://kstair.com/wp-content/uploads/2024/02/countertops-our-work.webp" alt="Wooden Countertops in Kitchen and Countertops Icon">
      </div>
      <div class="intro-text">
      <h2>Custom Wood Countertops</h2>
      <p>Wood countertops bring incredible presence and balance to any space. Discover how our countertops are made from start to finish. </p>
                <a href="custom-countertops">
                <div class="work-btn">
                <p>See Our Featured Project</p>
                </div>
                </a>
      </div>
      </div>
      
      </div>
      <div id="our-work-design-center">
        <div class="container">
            <div class="home-page-text">
                <h2>Our Design Center</h2>
                <p>Ours is a one-stop design center, a cutting-edge concept center that's capable of producing a range of looks from traditional to contemporary. During the design and concept phase, our sales staff, designers and support staff work with your builder, architect, and designer to translate your ideas from paper possibilities into digital realities. This collaborative approach helps ensure your design is expertly cared for and understood by craftsmen who build your countertops, stairs, millwork, doors and windows.</p>
            </div>
        </div>
        <div class="back-shadow"></div>
        <div style="clear:both;"></div>
      </div>
      <div class="call-out-bar">
        <p><a href="/contact">Interested? Click here for an estimate.</a></p>
      </div>
      </div>
        

       <?php /* while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'content', 'page' );  ?>

        <?php endwhile; // end of the loop.*/  ?>
        </main><!-- #main -->
    </div><!-- #primary -->
    

<?php get_footer(); ?>
