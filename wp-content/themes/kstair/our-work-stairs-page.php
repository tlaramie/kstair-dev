<?php /*Template Name: Our Work - Custom Stairs*/?>

<?php get_header(); ?>

<!-- Featured Background-->
        <div id="top"></div>
		<div class="fullscreen" id="work-stairs-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <h1 class="featured-title italic">Grand Staircase at Big Cedar Lodge</h1>
        <p><em>"This is the story of how one of the most breathtaking pieces we’ve ever created came to be. It was unexpected, an unreal timeline, and an unbelievable effort of craftsmanship and collaboration. I hope you enjoy it."</em></p>
        <img src="<?php bloginfo('template_directory'); ?>/images/brian-signature-sm-white.png"> 
        <p>President<br/>Kirkwood Stair &amp; Millwork</p>
		<div class="scrolling-arrow">
        	<p class="center"><a href="#scrolling-content">Scroll Down</a></p>
        	<a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
        </div>
        </div>
        </div>
        </div>
        
        <div id="scrolling-content"></div>

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        	<div class="multiple-phone">
				<div class="phone">
					<p>Windows &amp; Doors <br/> <a href="tel:314.781.5151">314.781.5151</a></p>
				</div>
				<div class="phone">
					<p>All other inquiries <br/> <a href="tel:636.271.4002">636.271.4002</a></p>
				</div>
			</div>
			<div class="container">
				<div class="newsletter-ad">
					<p class="subscribe">Subscribe</p>
					<p class="italic">Stay in touch and receive exclusive offers.</p>
					<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
					<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
				</div>
				<div class="contact-ad">
					<p class="subscribe">Contact</p>
					<p class="italic">Contact us with any questions and comments.</p>
					<a href="/contact">
						<div class="contact-btn">
							<p>Request an Estimate</p>
						</div>
					</a>
				</div>
			</div>
        </div><!-- end of .action bar -->
        
        <div id="project-categories">
<a href="index.php?page_id=168"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 90 90" enable-background="new 0 0 90 90" xml:space="preserve">
<circle fill="#C7B299" cx="45" cy="45.2" r="44"/>
<g>
	<path fill="#42322B" d="M67.2,24.4H21.9L7.1,58h75.9L67.2,24.4z M23.6,54.7l5.4-18h21l3.3,18H23.6z M57.7,47.6h-2.3v-2.9h-2.1v-2.9
		h2.2V31.3c0-1.3-1.4-2.6-3.5-2.6c-2.1,0-3.3,1.4-3.3,3.3l-1.9,0c0-1.1,0.6-5.9,5.1-5.9c4.5,0,5.8,2.9,5.8,4.7V47.6z"/>
	<rect x="7.1" y="58.9" fill="#42322B" width="75.9" height="1.8"/>
</g>
</svg></a>

<svg class="current" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 90 90" enable-background="new 0 0 90 90" xml:space="preserve">
<circle fill="#C7B299" cx="45" cy="45.2" r="44"/>
<g>
	<polygon fill="#42322B" points="26.3,18.8 24.7,21.5 65.1,21.5 63.8,18.8 	"/>
	<rect x="24.7" y="21.9" fill="#42322B" width="40.5" height="5.9"/>
	<polygon fill="#42322B" points="23.7,29.5 21.8,32.5 67.8,32.5 66.3,29.5 	"/>
	<rect x="21.8" y="33" fill="#42322B" width="46.1" height="6.7"/>
	<polygon fill="#42322B" points="20.5,41.4 18.3,45 71.4,45 69.7,41.4 	"/>
	<rect x="18.3" y="45.6" fill="#42322B" width="53.1" height="7.7"/>
	<polygon fill="#42322B" points="18.2,55.2 15.8,59.1 74.6,59.1 72.7,55.2 	"/>
	<rect x="15.8" y="59.9" fill="#42322B" width="58.9" height="8.2"/>
</g>
</svg>

<!--<a href="https://www.pinterest.com/kirkwoodstair/" target="_blank">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 120 120" enable-background="new 0 0 120 120" xml:space="preserve">
<circle fill="#C7B299" cx="60" cy="60.3" r="58.7"/>
<g>
	<path fill="#43332B" d="M56.4,69.7c-0.5,2.1-1.1,3.9-1.4,5.8c-1.1,5.3-3.4,9.9-6.8,14.3c-0.1,0.1-0.1,0.2-0.2,0.3
		c-0.2,0.1-0.4,0.2-0.5,0.2c-0.1,0-0.2-0.3-0.3-0.4c-0.2-1.7-0.4-3.5-0.5-5.3c-0.3-4.1,0.7-8,1.6-11.9c1.1-4.5,2.2-8.9,3.2-13.4
		c0.1-0.3,0-0.8-0.1-1.1c-1-2.9-1.2-6-0.1-8.8c0.8-2.2,2.1-3.9,4.5-4.6c3.2-0.9,5.8,1.1,5.9,4.3c0.1,2.1-0.4,4-1,6
		c-0.7,2.3-1.4,4.5-2,6.8c-0.9,3.7,2,7,5.8,6.6c2.9-0.2,5-1.9,6.6-4.1c2-2.7,2.8-5.8,3.4-8.8c0.3-2.3,0.5-4.5,0.3-6.8
		c-0.7-5.6-3.9-9.4-9.3-10.7c-5.9-1.5-11.5-0.7-16.1,3.5c-4.5,4-6.1,8.9-5.1,14.9c0.2,1.4,1,2.7,2,3.9c0.5,0.7,0.8,1.3,0.4,2.1
		c-0.2,0.8-0.4,1.6-0.7,2.4c-0.3,1.1-0.9,1.3-1.9,0.9c-2.7-1.2-4.6-3.3-5.6-6.1C36,53,37.1,46.7,41,41.3c3.5-5,8.3-7.9,14.2-9.1
		c4.9-1,9.7-1,14.5,0.9c6.5,2.5,10.8,7,12.5,13.7c0.7,2.5,0.5,5.1,0.3,7.7c-0.4,3.8-1.3,7.4-3.3,10.6c-2.4,4.3-5.8,7.3-10.5,8.5
		c-2.8,0.8-5.8,0.9-8.4-0.3C58.8,72.4,57.5,71.4,56.4,69.7z"/>
</g>
</svg></a>-->
        
        <!--<img id="svg-style" src="<?php bloginfo('template_directory'); ?>/images/countertop-icon.svg" alt="Custom Wood Countertops">
        <img src="<?php bloginfo('template_directory'); ?>/images/stairs-icon.svg" alt="Custom Stairs">
        <a href="http://www.pinterest.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/pinterest-icon.svg" alt="Follow us on Pinterest"></a>-->
        
        <a href="https://www.pinterest.com/kirkwoodstair/" target="_blank"><div class="pinterest-call container" id="stairs-pinterest">
        <p>Follow us for more examples of our custom stairs.</p>
        </div></a>
        </div>
                       
        <div class="featured-gallery-container">
       	   <div class="gallery-control prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56.63 88.13"><path d="M36.71,93a3.47,3.47,0,0,1-4.92,0L22.71,84a3.47,3.47,0,0,1,0-4.92l29-29-29-29a3.47,3.47,0,0,1,0-4.92L31.79,7a3.47,3.47,0,0,1,4.92,0L77.29,47.54a3.47,3.47,0,0,1,0,4.92Z" transform="translate(-21.69 -5.94)" style="fill:#fff"/></svg></div>
       	   <div class="gallery-control next active"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56.63 88.13"><path d="M36.71,93a3.47,3.47,0,0,1-4.92,0L22.71,84a3.47,3.47,0,0,1,0-4.92l29-29-29-29a3.47,3.47,0,0,1,0-4.92L31.79,7a3.47,3.47,0,0,1,4.92,0L77.29,47.54a3.47,3.47,0,0,1,0,4.92Z" transform="translate(-21.69 -5.94)" style="fill:#fff"/></svg></div>
			<div class="featured-gallery" id="stairs-gallery">
				<div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsA-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsA-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsA-l.jpg"/>
						</picture>
						<div class="gallery-text-wrapper" id="stairs-intro-text">
						<!--<img id="left-quotes" src="<?php bloginfo('template_directory'); ?>/images/left-quotes.png">
						<p><span class="italic">This is the story of how one of the most breathtaking pieces we’ve ever created came to be.  It was unexpected, an unreal timeline, and an unbelievable effort of craftsmanship and collaboration. I hope you enjoy it.</span></p>
						<img id="brian-sig" src="<?php bloginfo('template_directory'); ?>/images/brian-signature.png">
						<p>President<br/>Kirkwood Stair &amp; Millwork</p>-->
						<p class="story-action-text border-bottom"><br/><br/>Continue scrolling to the right to see the whole process &gt;</p>
						<p class="story-action-text"><a href="#story-text-scroll"> Or click here to read the full story &gt;</a></p></div>
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsB-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsB-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsB-l.jpg"/>
						</picture>
						<div class="gallery-text-wrapper" id="stairs-slide2-text">
						<div id="brown-text-bar">
						<p>We received a call from a contractor in August 2013 to come down to<br/><span class="bold"> Big Cedar Lodge in Branson, MO.</span></p>
						</div>
						<p class="italic">The time frame...Six to eight weeks!</p>
						</div>
					</div>
					<div class="featured-gallery-post text-block-brown">
						<div class="gallery-text-wrapper">
						<p>Because of the elements (and because it was a direct path to and from the spa), we suggested sealing all exposed wood and creating a wire-brush finish on the treads to eliminate slipping with bare feet and spa slippers.<br/><br/><span class="bold">We planned to hand-plane everything, which was also a huge challenge, given the curvature of the stairs.</span></p>
						</div>
					</div>          
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsC-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsC-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsC-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post text-block-tan">
						<div class="gallery-text-wrapper">
						<p><span class="bold">Two opposing, self-supporting circular stairs</span> rise from the platform into two more platforms on the upper area. Each of these platforms – one on the left and one on the right – lead you to the first floor, which features majestic views of Table Rock lake and the Big Cedar property.</p>
						</div>
					</div>  
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsD-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsD-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsD-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsE-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsE-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsE-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post text-block-brown">
						<div class="gallery-text-wrapper">
						<p>We calculated the amount of lumber we needed <span class="bold">using a sophisticated 3-D software</span> that took into account every piece – risers, treads, thicknesses, widths, lengths -- then began cutting and pre-finishing.</p>
						</div>  
					</div>
					<div class="featured-gallery-post timelapse">
						<script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
						<video id="video1" controls preload="auto" width="100%" height="100%" poster="https://kstair.com/wp-content/uploads/2023/08/kstair-custom-stairs-video-thumb-build.jpg" ></video>
						<script>
						  var video = document.getElementById("video1");
						  var videoSrc = "https://d2hqlhbe1d8j2s.cloudfront.net/buckeyeinternational/segment/51744f2677f74580aba36627389f5dd8/hls/1691179909947/51744f2677f74580aba36627389f5dd8.m3u8";
						  if (video.canPlayType("application/vnd.apple.mpegurl")) {
						    video.src = videoSrc;
						  } else if (Hls.isSupported()) {
						    var hls = new Hls();
						    hls.loadSource(videoSrc);
						    hls.attachMedia(video);
						    hls.on(Hls.Events.MANIFEST_PARSED, function () {
						      video.play();
						    });
						  }
						</script>

					<!--<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><iframe src="//fast.wistia.net/embed/iframe/h2gl6iw10n?videoFoam=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="100%" height="100%"></iframe></div></div>
	<script src="//fast.wistia.net/assets/external/E-v1.js" async></script>-->
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsF-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsF-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsF-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post text-block-tan">
						<div class="gallery-text-wrapper">
						<p>We also made a critical decision: <span class="bold">to build and assemble the staircase in our shop</span>, truck it down to Big Cedar, then install it by crane.</p>
						</div>
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsG-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsG-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsG-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post text-block-brown">
						<div class="gallery-text-wrapper">
						<p><span class="bold">It took 12 guys</span> to load the finished staircase onto the truck, and more than a few hours to transport it.</p>
						</div>
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsH-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsH-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsH-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsI-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsI-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsI-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post text-block-tan">
						<div class="gallery-text-wrapper">
						<p><span class="bold italic">The crane slooooowly</span> lifted the staircases off the truck and guided them through the window.</p>
						</div>
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsJ-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsJ-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsJ-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsK-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsK-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsK-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post timelapse">
						<script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
						<video id="video2" controls preload="auto" width="100%" height="100%" poster="https://kstair.com/wp-content/uploads/2023/08/kstair-custom-stairs-video-thumb-install.jpg" ></video>
						<script>
						  var video = document.getElementById("video2");
						  var videoSrc = "https://d2hqlhbe1d8j2s.cloudfront.net/buckeyeinternational/segment/77843b42fef34dea984b3e5bd531adb5/hls/1691179882078/77843b42fef34dea984b3e5bd531adb5.m3u8";
						  if (video.canPlayType("application/vnd.apple.mpegurl")) {
						    video.src = videoSrc;
						  } else if (Hls.isSupported()) {
						    var hls = new Hls();
						    hls.loadSource(videoSrc);
						    hls.attachMedia(video);
						    hls.on(Hls.Events.MANIFEST_PARSED, function () {
						      video.play();
						    });
						  }
						</script>

					<!--<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><iframe src="//fast.wistia.net/embed/iframe/wrwj9ubjut?videoFoam=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="100%" height="100%"></iframe></div></div>
	<script src="//fast.wistia.net/assets/external/E-v1.js" async></script>-->
					</div>  
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsM-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsM-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsM-l.jpg"/>
						</picture>
					</div> 
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsL-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsL-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsL-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsN-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsN-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsN-l.jpg"/>
						</picture>
					</div>
					<div class="featured-gallery-post" id="final-stairs-picture">
						<picture>
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsO-l.jpg" media="(min-width: 769px)" />
							<source srcset="<?php bloginfo('template_directory'); ?>/images/work-stairsO-s.jpg" media="(max-width: 769px)" />
							<img src="<?php bloginfo('template_directory'); ?>/images/work-stairsO-l.jpg"/>
						</picture>
					</div>                                     
				</div>
			</div> <!--End of featured-gallery-->
		</div>
       
        <div id="story-text-scroll"></div> 
        <div class="work-story-text">
        <div class="container">
        	<h2>The Big Cedar Installation</h2>
            <p class="italic bold">This is the story of how one of the most breathtaking pieces we’ve ever created came to be.  It was unexpected, an unreal timeline, and an unbelievable effort of craftsmanship and collaboration. I hope you enjoy it.</p>
            <p class="italic bold">Brian Berger<br/>President<br/>Kirkwood Stair &amp; Millwork</p>
            <p>We received a call from a contractor in August 2013 to come down to Big Cedar Lodge in Branson, MO.</p>
            <p>The contractor explained they had a grand staircase that was posing a huge challenge.  The stairs were designed in steel and concrete with a wood veneer, and that the total job was way over budget. They needed engineering advice and, after careful consideration and lengthy conversation, we felt we could make this grand statement out of all wood – and make it just as sound as steel.</p>
            <p>The price? Wood was nearly half that of the steel and concrete and, frankly, double that in beauty. The time frame? Six to eight weeks! Adding to that, the contractor was under intense pressure by the owner to get the job done.</p>
            <p>Our team (me, our production engineer and salesman) all met with the contractor on site. As we talked, we started narrowing down species and settled on quarter-sawn white oak. It was the most durable choice, particularly considering the environmental factors, like: west and south sun exposure through huge windows, along with fountains and a reflective pool directly under the stair case that would impact the stability of the wood causing cracking, warping, fading and more.</p>
            <p>Because of the elements (and because it was a direct path to and from the spa), we suggested sealing all exposed wood and creating a wire-brush finish on the treads to eliminate slipping with bare feet and spa slippers. We planned to hand-plane everything, which was also a huge challenge, given the curvature of the stairs.</p>
            <p>We also made a critical decision: to build and assemble the staircase in our shop, truck it down to Big Cedar, then install it by crane.</p>
            <p>The actual staircase design is made up of four bowed risers lifting up to a large platform that is actually standing over the reflecting pool with the fountains underneath. Two opposing, self-supporting circular stairs rise from the platform into two more platforms on the upper area.  Each of these platforms – one on the left and one on the right – lead you to the first floor, which features majestic views of Table Rock lake and the Big Cedar property.</p>
            <p>We started production with laser measuring devices to determine exact heights and dimensions of all walls, headers and floor heights After completing our measurements, we got a call from the contractor with a special request.</p>
            <p>Their engineering department wanted to see our load tests for the wood staircase in order to meet code.  Load tests?  Wow, we’ve never done load tests on our staircases.  We’ve been building stairs for decades and the tradition of stair building has been in existence for thousands of years.  We’ve followed generations of proven steps and procedures, knowing they’d hold up.</p>
            <p>But all of our experience…is residential, which doesn’t fly with commercial contractors who need load specifications.  Worse, there were no existing designs that could be used comparatively for the load test. The only way we could prove a 100-pound per square foot load...was to do it ourselves.</p>
            <p>We built a 14-riser prototype staircase in the shop and used  cinderblocks to replicate the load. The staircase was required to hold the load for 24 hours.  There was a little creaking, but we adjusted for it and passed the test with the engineer’s seal of approval.  From there, we calculated the amount of lumber we needed using sophisticated 3-D software that took into account every piece – risers, treads, thicknesses, widths, lengths -- then began cutting and pre-finishing.</p>
            <p>We started building the stairs in our shop from the bottom up, installing the treads and risers into the finished stringers up to a temporary header.  We bolted these parts together and then  bolted the structural carriages to the decorative wall stringers.  We then installed the outside face stringer over the structural carriage.  Once we put the finished cap on, our master craftsmen hand-planed and sanded all parts of the assembly so stringers holding the treads, risers and railings would look and feel like one large, thick solid beam.</p>
            <p>The logistics for transporting the staircase were nearly as challenging as the build itself. It took 12 guys to load the finished staircase onto the truck, and more than a few hours to transport it. Once there, the crane slooooowly lifted the staircases off the truck and guided them through the window.</p>
            <p>After that? A little thing called assembly. We’ll spare the details; safe to say it went well.</p>
            <p>Hope you enjoyed the ride. We sure did.</p>
            <p>Brian</p>
        </div>
        </div>
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
