<?php /*Template Name: Careers*/?>


<?php get_header(); ?>
		

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>314.781.5151</ph>
            <p class="italic">Contact us with any questions and comments. </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
		<div id="careers-content">
        <div class="title-image">
        <div class="container">
        <h2>Careers</h2>
        <h3>Come work with us at Kirkwood Stair.</h3>
        </div>
        </div>
        <div class="container">
       	<?php  while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.  ?>
        </div>
        <div class="container">
        <h1>Available Positions</h1>
        <br/><br/>
	 	<?php query_posts('cat=14&amp;showposts='.get_option('posts_per_page')); ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
        
		
		<?php
		$categories = get_the_category();
		$excluded_cat = '14';
		$separator = ' / ';
		$output = '';
		if($categories){
			foreach($categories as $category) {
				if (
					$category->cat_ID != $excluded_cat
				)
					$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
			}
			echo trim($output, $separator);
		}
		?>

			<h2><a href="<?php the_permalink() ?>"> <?php the_title(); ?></a></h2>

			<?php include (TEMPLATEPATH . '/_/inc/meta.php' ); ?>

			<div class="entry">
            <?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  			the_post_thumbnail();
			}
			?>
            <?php the_excerpt( $more_link_text , $strip_teaser ); ?> 
			</div>	
		
		</article>

	<?php endwhile; ?>
	<br/><br/>
	<h1>Submit your application</h1>

	<?php gravity_form( 5, true, true, false, false, false, false); ?>

	<?php include (TEMPLATEPATH . '/_/inc/nav.php' ); ?>

	<?php else : ?>

		<p><?php _e( 'No open positions available at this time.  Thank you for your interest.'); ?></p>

	<?php endif; ?>

        </div>
        </div>
        
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
