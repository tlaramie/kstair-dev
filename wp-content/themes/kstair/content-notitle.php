<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package KStair
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'kstair' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	</div><!-- end of container -->
	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'kstair' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
