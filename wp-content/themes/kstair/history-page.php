<?php /*Template Name: History Page*/?>

<?php get_header(); ?>

<!-- Featured Background-->
		<div id="top"></div>
		<div class="fullscreen" id="history-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <h1 class="featured-title italic">History of a Craftsman</h1>
        <p>Creating stairs, millwork, and doors by hand has always been the work of artisans. Located in Pacific, Missouri and set along the hardwood forests and limestone hills by the scenic Meramec River is our factory of artisans.</p>
		<div class="scrolling-arrow">
        	<p class="center"><a href="#scrolling-content">Scroll Down</a></p>
        	<a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
        </div>
        </div>
        </div>
        </div>
        
        <div id="scrolling-content"></div>
		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        	<div class="multiple-phone">
				<div class="phone">
					<p>Windows &amp; Doors <br/> <a href="tel:314.781.5151">314.781.5151</a></p>
				</div>
				<div class="phone">
					<p>All other inquiries <br/> <a href="tel:636.271.4002">636.271.4002</a></p>
				</div>
			</div>
			<div class="container">
				<div class="newsletter-ad">
					<p class="subscribe">Subscribe</p>
					<p class="italic">Stay in touch and receive exclusive offers.</p>
					<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
					<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
				</div>
				<div class="contact-ad">
					<p class="subscribe">Contact</p>
					<p class="italic">Contact us with any questions and comments.</p>
					<a href="/contact">
						<div class="contact-btn">
							<p>Request an Estimate</p>
						</div>
					</a>
				</div>
			</div>
        </div><!-- end of .action bar -->       
        
       <!--Start of featured-gallery-->
       <div class="featured-gallery-container">
       	   <div class="gallery-control prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56.63 88.13"><path d="M36.71,93a3.47,3.47,0,0,1-4.92,0L22.71,84a3.47,3.47,0,0,1,0-4.92l29-29-29-29a3.47,3.47,0,0,1,0-4.92L31.79,7a3.47,3.47,0,0,1,4.92,0L77.29,47.54a3.47,3.47,0,0,1,0,4.92Z" transform="translate(-21.69 -5.94)" style="fill:#fff"/></svg></div>
       	   <div class="gallery-control next active"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56.63 88.13"><path d="M36.71,93a3.47,3.47,0,0,1-4.92,0L22.71,84a3.47,3.47,0,0,1,0-4.92l29-29-29-29a3.47,3.47,0,0,1,0-4.92L31.79,7a3.47,3.47,0,0,1,4.92,0L77.29,47.54a3.47,3.47,0,0,1,0,4.92Z" transform="translate(-21.69 -5.94)" style="fill:#fff"/></svg></div>
		   <div class="featured-gallery" id="history-gallery">
				<div>
						<div class="featured-gallery-post">
							<picture>
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyA-l.jpg" media="(min-width: 769px)" />
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyA-s.jpg" media="(max-width: 769px)" />
								<img src="<?php bloginfo('template_directory'); ?>/images/historyA-l.jpg"/>
							</picture>
							<div class="gallery-text-wrapper" id="history-directions">
							<p>Scroll right to follow along &gt;</p>
							</div>
							<div class="gallery-text-wrapper" id="historyA">
								<h2>1899</h2>
								<p>Our roots were planted in Highland, Illinois.</p>
							</div>
						</div>
					   <div class="featured-gallery-post">
							<picture>
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyB-l.jpg" media="(min-width: 769px)" />
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyB-s.jpg" media="(max-width: 769px)" />
								<img src="<?php bloginfo('template_directory'); ?>/images/historyB-l.jpg"/>
							</picture>
							<div class="gallery-text-wrapper" id="historyB">
								<h2>Early 1900's</h2>
								<p>We moved to Kirkwood, Missouri and became Kirkwood Stair Company.</p>
							</div>
					   </div>
						<div class="featured-gallery-post">
							<picture>
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyC-l.jpg" media="(min-width: 769px)" />
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyC-s.jpg" media="(max-width: 769px)" />
								<img src="<?php bloginfo('template_directory'); ?>/images/historyC-l.jpg"/>
							</picture>
							<div class="gallery-text-wrapper" id="historyC">
								<h2>1900s – 1970’s </h2>
								<p>We were really busy.</p>
							</div>
						</div>
						<div class="featured-gallery-post">
							<picture>
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyD-l.jpg" media="(min-width: 769px)" />
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyD-s.jpg" media="(max-width: 769px)" />
								<img src="<?php bloginfo('template_directory'); ?>/images/historyD-l.jpg"/>
							</picture>
							<div class="gallery-text-wrapper" id="historyD">
								<h2>Late 1970s </h2>
								<p>We had significant growth and expansion, suddenly going from humble to huge. </p>
							</div>
						</div>
						<div class="featured-gallery-post">
							<picture>
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyE-l.jpg" media="(min-width: 769px)" />
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyE-s.jpg" media="(max-width: 769px)" />
								<img src="<?php bloginfo('template_directory'); ?>/images/historyE-l.jpg"/>
							</picture>
							<div class="gallery-text-wrapper" id="historyE1">
								<h2>1982 </h2>
								<p>The Meramec River flooded and submerged all of our machines, lumber, and inventory. </p>
							</div>
							<div class="gallery-text-wrapper" id="historyE2">
								<h2>1984</h2>
								<p>We emerged bigger and stronger, and moved to our current 40,000 square foot facility in Pacific, Missouri. </p>
							 </div>
						</div>
						<div class="featured-gallery-post">
							<picture>
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyF-l.jpg" media="(min-width: 769px)" />
								<source srcset="<?php bloginfo('template_directory'); ?>/images/historyF-s.jpg" media="(max-width: 769px)" />
								<img src="<?php bloginfo('template_directory'); ?>/images/historyF-l.jpg"/>
							</picture>
							<div class="gallery-text-wrapper" id="historyF">
								<h2>Now</h2>
								<p>We work with clients throughout the Midwest, offering artisan quality, passion and creativity.</p>
							</div>
						</div>
				</div>       
			</div> <!--End of featured-gallery-->
		</div>
        
		<div class="call-out-bar">
        <p>We build stairs, carve mouldings, and make doors whose beauty will stand proudly for hundreds of years in some of America’s most remarkable homes.</p>
        </div>
        <div id="associates">
         <a href="http://www.wpma.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/wood-products-manufacturers.jpg" alt="Kirkwood Stair & Millwork"></a>
         <a href="http://www.carpdc.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/carpenters-district-council.jpg" alt="Kirkwood Stair & Millwork"></a>
         <a href="http://www.cbgstl.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/certified-builders-guild.jpg" alt="Kirkwood Stair & Millwork"></a>
         <a href="http://www2.epa.gov/lead/epa-lead-safe-certification-program" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/lead-safe-certified.jpg" alt="Kirkwood Stair & Millwork"></a>
         <a href="http://www.stlhba.com/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/hba.jpg" alt="Kirkwood Stair & Millwork"></a>
        </div>
        
       <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
