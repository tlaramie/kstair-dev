<?php /*Template Name: Stairs*/?>

<?php get_header(); ?>

<!-- Featured Background-->
		<div id="top"></div>
		<div class="fullscreen" id="stairs-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <h1 class="featured-title italic">Wood Stairs and Stair Parts</h1>
        <p>No other interior architectural element makes a stronger statement than your staircase. Since 1899, Kirkwood Stair &amp; Millwork has been helping builders, contractors, homeowners, and designers realize their visions into timeless architectural works through, simply, wood artistry and craftsmanship.</p>
        <p>To see examples of our custom stairs, click <a href="#stairsCarousel" class="bold">here.</a></p>
        
        <div class="scrolling-arrow">
        	<p class="center"><a href="#scrolling-content">Scroll Down</a></p>
        	<a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
        </div>
        
        </div>
        </div>
        </div>
        
        <div id="scrolling-content"></div>

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>636.271.4002</ph>
            <p class="italic">Interested in a custom staircase? </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an Estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
		<div id="stair-planning">
        <div class="container">
        <div class="intro-text">
        <h2>Stair Planning and Design</h2>
        <p>We provide design-build services, direct from the manufacturer. No matter size nor scope, our team of professionals, craftsmen, and partner companies will creatively solve any structural or design challenges from initial design to the finished installation.</p>
        <p>Blending form, function, engineering, and architectural nuances is what we do. All wood, metal, glass and stone can be flowed individually or jointly into a staircase. The three-dimensional curves, turns, bends and free-flowing railings and stair parts offer endless visual appeal. There’s really no place else in a space where you can make a statement so dramatically. Contact us. Let’s work together to design, budget, order, and complete your project.</p>
        <p id="stairs-consultation-note"><em>Stair consultations by appointment only</em></p>
        </div>
        <div class="icon-callouts">
        	<div>
        	<a href="/our-work/custom-stairs"><img src="<?php bloginfo('template_directory'); ?>/images/stairs-icon.svg" alt="Stairs Process"></a>
        	<p class="bold"><a href="/our-work/custom-stairs">See how our stairs are made from start to finish.</a></p>
        	</div>
        	<div>
        	<a href="http://www.pinterest.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/pinterest-icon.svg" alt="Follow us on Pinterest"></a>
        	<p class="bold"><a href="http://www.pinterest.com/kirkwoodstair/" target="_blank">Follow us on Pinterest to see custom stairs and more.</a></p>
            </div>
        </div>
        <div id="balusters">
        <img src="<?php bloginfo('template_directory'); ?>/images/baluster-pic-min.png" alt="Baluster Replacement Program">
        <div id="baluster-text">
        <h2>Baluster Replacement Program</h2>
        <p>Whether you’re installing new stairs and stair parts, or interested in enhancing your existing stairs, new wood or iron balusters (also called spindles or stair sticks) can be transformative. </p>
        <p>You can mix and match balusters and iron panels with endless design options, patterns and finishes to create a stunning new look for your foyer. If you’d like to experiment in our virtual design center -- by dragging and dropping a variety of baluster designs onto stairs or balconies -- visit our partner vendors or contact us directly to learn more.</p>
        <p>Our Baluster Replacement Program is simple and speedy. In most cases, your order is ready for installation in two weeks or less. The installer completes all cutting outdoors, disposes of all old balusters and stair parts, and cleans and removes any dust or debris. In fact, the only sign that we'll have been there will be the new work we’ve installed.</p>
        </div>
        </div>
        </div>
        </div>
        
        
    <div id="stairsCarousel" class="carousel slide">
    <!--<ol class="carousel-indicators">
    <li data-target="#countertopCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#countertopCarousel" data-slide-to="1"></li>
    <li data-target="#countertopCarousel" data-slide-to="2"></li>
    </ol>-->
    <!-- Carousel items -->
    <div class="carousel-inner lazy-slider" data-slides='["stairsK","stairsC", "stairsA", "stairsG", "stairsH", "stairsI", "stairsJ", "stairsB", "stairsD", "stairsE"]'>
		<div class="active item">
            <picture>
                <source srcset="<?php bloginfo('template_directory'); ?>/images/lazy-load-carousel-placeholder-l.gif" media="(min-width: 1201px)" />
                <source srcset="<?php bloginfo('template_directory'); ?>/images/lazy-load-carousel-placeholder-m.gif" media="(min-width: 601px)" />
                <source srcset="<?php bloginfo('template_directory'); ?>/images/lazy-load-carousel-placeholder-s.gif" media="(max-width: 600px)" />
                <img src="<?php bloginfo('template_directory'); ?>/images/lazy-load-carousel-placeholder-l.gif" />
            </picture>
        </div>
    </div>
    <!-- Carousel nav -->
    <a class="carousel-control left" href="#stairsCarousel" data-slide="prev"><img src="<?php bloginfo('template_directory'); ?>/images/pg-left.svg"></a>
    <a class="carousel-control right" href="#stairsCarousel" data-slide="next"><img src="<?php bloginfo('template_directory'); ?>/images/pg-right.svg"></a>
    </div>
    <div class="complement-doors-windows">
    	<header>
        	<h2>Complement Your Stairs</h2>
        	<p>Complement your stairs with Kirkwood Stair &amp; Millwork’s additional services. Stairs can make a strong statement in any space, but don’t let them stand alone. Piece them together with custom windows and doors, sleek wood countertops, and striking millwork with the help of our team.</p>
		</header>
        <div class="container">
            <div class="callout">
                <div class="image">
                    <img src="<?php bloginfo('template_directory'); ?>/images/wood-countertop-callout.jpg" alt="Wood Countertop Callout" />
                </div>
                <div class="text">
                    <h3>Wood Countertops</h3>
                    <p>Discover the beauty and durability of natural wood countertops for your kitchen, islands, and bars. All of our wood countertops are completely hand sanded, then protected and finished with Waterlox® tung oil to create a lasting, lustrous and food-prep safe surface.</p>
                    <p><a href="/wood-countertops/">More about wood countertops &gt;</a></p>
                </div>
            </div>
            <div class="callout">
                <div class="image opp">
                    <img src="<?php bloginfo('template_directory'); ?>/images/windows-doors-callout.jpg" alt="Windows and Doors Callout" />
                </div>
                <div class="text opp">
                    <h3>Windows &amp; Doors</h3>
                    <p>Customization is vital to making your space unique. That’s why we are a proud distributor of Kolbe Windows &amp; Doors, an industry leader in custom windows and doors designed to fit any unique vision. You bring the idea, and our knowledgeable staff will help you choose the right Kolbe products for your project.</p>
                    <p><a href="/doors-windows/">More about windows &amp; doors &gt;</a></p>
                </div>
            </div>
            <div class="callout">
                <div class="image">
                    <img src="<?php bloginfo('template_directory'); ?>/images/mouldings-callout.jpg" alt="Mouldings Callout" />
                </div>
                <div class="text">
                    <h3>Custom Millwork</h3>
                    <p>From arched casings to crown moulding, bookcases to library paneling, hand-hewn beams to mantles, we've got your custom millwork needs covered. Our collaborative approach ensures that your ideas reach their full potential.</p>
                    <p><a href="/custom-millwork/">More about custom millwork &gt;</a></p>
                </div>
            </div>
        </div>
	</div>
    <div class="call-out-bar" style="margin-top:0;">
        <p><a href="/contact">Interested in custom stairs? Click here for an estimate.</a></p>
    </div>
        
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
