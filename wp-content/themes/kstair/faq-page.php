<?php /*Template Name: FAQ*/?>


<?php get_header(); ?>
		

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>314.781.5151</ph>
            <p class="italic">Contact us with any questions and comments. </p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request an estimate</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
		<div id="faq-content">
        <div class="title-image">
        <div class="container">
        <h2>FAQ</h2>
        </div>
        </div>
        <div class="container">
        	<h3>When designing a new home, isn't the kitchen or master bath budget more important than the stairs?</h3>
            <p>No other interior architectural element makes a stronger statement than your staircase. Sure, the kitchen is often the “centerpiece” (and we have some amazing countertops – but that’s another topic), but the first thing your guests see is the staircase – it’s your first elegant, bold, daring or subtle personal statement. The ability to blend form, function, engineering and architectural nuances cannot be found anyplace else in your home. All the natural wood, metal, glass and stone elements can be incorporated individual or jointly into your staircase. The three-dimensional aspects of curves, turns, bends and free-flowing railings offer endless visual appeal.</p>
            <h3>How do we get started designing a staircase?</h3>
            <p>Once you’re ready, it’s important to determine the style and design of your stairs. Our website is the perfect place to start reviewing different designs and shapes of stairs. Articles, magazine pictures, photographs, etc. are also great resources for starting the process. National and local building magazines like Architectural Digest can provide great suggestions for designs and styles. Sit down with your architect and/or designer and peruse our website along with the pictures and information you’ve gathered. Be open to substituting different building materials or shapes of the stairs within the same design as pictured. Don’t be afraid to blend the ideas of several different stairs into one staircase. Pick out the important features of the stairs that you see and incorporate them into your staircase.  In short, your staircase is an amazing creative and collaborative process.</p>
            <h3>What are some of the common design features and terms to look for in a staircase?</h3>
            <p>Break the stairs into two design features: the step-up and the railings. Look at how the two may or may not blend together. On the step-up, notice if the treads have returns on them (the nosing of the tread returns back on itself and, when looking at the side of the staircase, you can see a saw-tooth step-up of the treads and risers). The tread return may or may not have a tread bracket (decorative applique) applied to the face stringer (the open part of the staircase that the riser miters into and the tread sits on). In this common feature, the balusters (spindles) usually sit on tread with the handrail above it. In a curb string application, the balusters sit on a short wall. The face stringer is eliminated and you can’t see the tread or riser from the side of the stair. This application usually accompanies forged wrought iron railings as well as metal and wood balustrades and is trimmed with multiple layers of mouldings and millwork.</p>
            <h3>What’s next after choosing a design or theme?</h3>
            <p>Once you have the design, along with the building materials (wood, stone, glass, metal), it’s time to design the structure and shape of the stairs. Stairs come in many shapes: curved, circular, spiral, 90- degree platforms, 180-degree platforms, winder stairs, etc. Many of these shapes are dependent on where the staircase is being placed in the home and the amount of room that has been allotted for the stairs. Circular, straight run, and 90-degree platform stairs need a larger footprint than a switchback or winding staircase. Since a staircase is as much about function as it is design, local and national building codes will apply -- like run (tread) depths, riser heights, baluster spacings, rail heights, etc., which can limit small space applications for primary staircases. Secondary stairs, such as spirals, have more lenient codes and can be put into tighter spaces. The Kirkwood team has worked through many, many space challenges and can absolutely help you work your design into the stair shape that will best match and enhance your space, while meeting required codes. We’ll work closely with your architect and designer to artfully blend your staircase into the millwork of your room and overall home nuances and style.</p>
            <h3>What if I’m building from a model display home or from a stock plan set…not a custom home?</h3>
            <p>Many of our engineering, code and stair shapes features will have been determined for you, BUT…you’ll still have wide latitude on design selections. We may even be able to re-shape your stairs depending on the structural and space limitations of the existing drawings.</p>
            <h3>What if I’m just remodeling and not building a new home?</h3>
            <p>Although you’ll be more limited to the existing space or shape of the stairs, there are many design options to choose from. We specialize in helping folks realize a new look for their home by re-working their existing staircase. Whether it’s a whole tear-out or simply replacing existing wood balusters with newly- designed turnings or metal balusters, we can create design ideas that you’d see in new and custom homes that were begun from scratch.</p>
            <h3>What is the process for quotes?</h3>
            <p>Once you’ve determined the design of your stairs and incorporated the shape into your plans, we can start the quoting process. We’ll use the architect’s plans or field measurements, then enter your staircase into our 3-D modeling software, CAD software, or draft a hand drawing/sketch. General, quick estimates and rail/baluster replacement jobs are usually done with the hand-drawn process due to the complexity of entering electronically-drawn stairs and lacking the requirement of engineering. A detailed material list is then generated and processed, along with an installation quote for a job-built staircase with or without a rough-in (field framing as the foundation of the staircase) or a shop built stair with optional installation. Clients with qualified stair installers and installers themselves can get material-only quotes upon request.</p>
            <h3>Is everything custom or can I pick from stock parts?</h3>
            <p>We have a huge selection of stock and semi-stock parts (two weeks or less delivery). There are four stocking lines of railings: Colonial, Provincial, English and French Country, along with dozens of metal baluster patterns to choose. Stocking species include Red Oak, Poplar and Yellow Pine. You can create a one-of-a-mind staircase using combinations of our stocking and standard-pattern stair parts found within our website and through some of our vendor partners.</p>
            <h3>How do I make a quote become an order?</h3>
            <p>If your quote meets all your design requirements and functional needs, then a signed proposal and deposit will be required to begin processing your order. On non-installed orders, the balance of the order will be required before shipping. On installed orders, we will require another payment prior to shipping the order, with the balance due 48 hours prior to our installer finishing the job. Full payment is required even if balusters and parts of the stairs are left off for painting purposes and requiring our installer to return to the jobsite at a future date.</p>
            <h3>After placing an order, what can I expect during the building phase of our home?</h3>
            <p>When we receive your signed proposal and deposit, we’ll begin processing your order in our shop.</p>
            <h3>What can I expect after our order is completed? </h3>
            <p>A Kirkwood representative will contact you to make sure everything went well with your order. Stair parts and installed joinery are warranted to be free of defects for one year from the date of completed installation. Kirkwood Stair &amp; Millwork must be notified of a defect within 24 hours of receipt of any non-installed parts and assembled stairs. Since our products are made of natural materials, slight anomalies such as pin knots, slight color variations of laminated wood, small checking and expansion/contraction joints at glue points are not considered defects. Kirkwood Stair &amp; Millwork is not responsible for damage resulting in jobsite abuse, environments that are not acclimatized -- whether occupied or not, or changes in jobsite dimensions after order has been placed and finished drawings have been authorized.</p>
        </div>
        </div>
        
        
        <div class="call-out-bar">
        <div class="container">
        <p><a href="/contact">Any lingering questions? Click here to have them answered.</a></p>
        </div>
        </div>
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
