<?php /*Template Name: Contact*/?>


<?php get_header(); ?>


		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        
        <div id="contact-content">
        <div class="container">
        <div id="contact-form">
        <h2>We Wood Love to Hear From You</h2>
        <p>If you have questions or would like to talk with one of our representatives, please call us, or complete the form below and we'll contact you.</p>
        <div class="multiple-phone">
			<div class="phone">
				<p style="line-height:1.2;"><a href="tel:314.781.5151">314.781.5151</a> <br/> for Windows &amp; Doors</p>
			</div>
			<div class="phone">
				<p style="line-height:1.2;"><a href="tel:636.271.4002">636.271.4002</a> <br/> for all other inquiries</p>
			</div>
		</div>
        <p id="contact-consultation-note"><em>Stair consultations by appointment only</em>.</p>
		<?php get_template_part( 'content', 'page' );  ?>
			<!--<div id="request-cad-form">
		   <iframe onload="$(this).height($(this.contentWindow.document.body).find(\'div\').first().height());" id="contactiframe" class="contactiframe" height="1400" allowTransparency="true" frameborder="0" scrolling="no" style="width:100%;border:none"  src="https://dzinewise.wufoo.com/embed/r169u7eu1jakmed/"><a href="https://dzinewise.wufoo.com/forms/r169u7eu1jakmed/">Fill out my Wufoo form!</a></iframe>
			</div>-->
        </div>
        <div id="contact-info">
        <a href="http://kstairquote.com/Quote/Step1" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/estimator-logo.svg" alt="Kirkwood Stair Quoting Engine"></a>
        <br/><br/>
        <p class="italic">Use this application to get an estimate for your custom wood countertop.</p>
        <hr/>
			<p><strong>Windows &amp; Doors</strong><br/>744 Hanley Industrial Court<br/>St. Louis, MO 63144<br/><span style="margin-top:0.25em;display:inline-block;">p: 314.781.5151</span></p>
			<p><strong>All Other Inquiries</strong><br/>50 Midwest Dr<br/>Pacific, MO 63069<br/><span style="margin-top:0.25em;display:inline-block;">p: 636.271.4002</span></p>
       	<hr/>
        <div class="contact-resource-callout">
        	<h2>Access CAD Files</h2>
            <p class="about">If you already have an account, log in to access your CAD files; otherwise, follow the request access link below.</p>
            <p><a href="https://www.kstairquote.com/resources/Home/Login" target="_blank">Log In</a></p>
            <p><a href="https://www.kstairquote.com/resources/Home/RequestAccess" target="_blank">Request Access</a></p>
        </div>
        <hr/>
        <div id="testimonials">
		<?php
		$args = array( 'posts_per_page' => 1, 'orderby' => 'rand', 'cat' => '9' );
		$rand_posts = get_posts( $args );
		foreach ( $rand_posts as $post ) : 
  		setup_postdata( $post ); ?>
			<?php the_content(); ?>
		<?php endforeach; 
		wp_reset_postdata(); ?>
        </div>
        
        </div>
        </div>
        </div>
                
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
 
 

<?php get_footer(); ?>
