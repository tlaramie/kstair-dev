<?php /*Template Name: Mouldings*/?>

<?php get_header(); ?>

<!-- Featured Background-->
<div id="top"></div>
<div class="fullscreen" id="mouldings-feature">
    <div class="feature-text">
        <div class="feature-text-wrapper">
            <h1 class="featured-title italic">Custom Millwork</h1>
            <p>Accentuate the natural beauty of any space with custom wood millwork to compliment unique windows, doors, and hallways. Take a standard fireplace mantle or custom built-in cabinet and turn it into a dramatic statement with our striking millwork.</p>
            <div class="scrolling-arrow">
                <p class="center"><a href="#scrolling-content">Scroll Down</a></p>
                <a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
            </div>
        </div>
    </div>
</div>



<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div id="scrolling-content"></div>
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
            <div class="container">
                <div class="newsletter-ad">
                    <p class="subscribe">Subscribe</p>
                    <p class="italic">Stay in touch and receive exclusive offers.</p>
                    <button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
                    <div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
                </div>
                <div class="contact-ad">
                    <ph>636.271.4002</ph>
                    <p class="italic">Interested in custom moulding? </p>
                    <a href="/contact">
                        <div class="contact-btn">
                            <p>Request an Estimate</p>
                        </div>
                    </a>
                </div>
            </div>
        </div><!-- end of .action bar -->

        <div id="mouldings">
            <div class="container">
                <h2 style="margin-bottom:0.25em;">Custom Millwork</h2>
                <p style="margin-bottom:3em;">Our millwork is made from the highest quality wood, and our craftsmen follow detailed and precise carving and manufacturing processes. We have a wide range of moulding profiles made from a variety of domestic and exotic hardwood. Stocking profiles include Poplar and Red Oak, Walnut, Mahogany, Cherry, Birch, Oak, Maple, Pine, Brazilian and American Cherry, Alder and many other species and sizes. Enhance your space with curved crowns, carvings, and arched casings with custom millwork.</p>
                <div class="intro-text">
                    <h2>Moulding Profiles for Every Taste and Space</h2>
                    <p>Our mouldings are made from strong, quality wood, and our craftsmen follow detailed and precise carving and manufacturing processes. Stocking profiles include Poplar and Red Oak, Walnut, Mahogany, Cherry, Birch, Oak, Maple, Pine, Brazilian and American Cherry, Alder and many other species and sizes in our full mouldings line. To learn more, download a PDF of our catalog. </p>
                    <p>All profiles can be transformed into arched casings, curved crowns and even carvings for virtually any millwork product you are looking for to enhance your space. We also match existing profiles and create new profiles with in-house knife grinding.</p>
                </div>
                <div class="intro-text">
                    <a href="https://www.kstairquote.com/resources/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/moulding-catalog-btn.svg"></a>
                </div>
                <div class="icon-callouts">
                    <div>
                        <a href="http://www.pinterest.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/pinterest-icon.svg" alt="Follow us on Pinterest"></a>
                        <p class="bold"><a href="http://www.pinterest.com/kirkwoodstair/" target="_blank">Follow us on Pinterest to see custom moulding and more.</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div id="mouldingCarousel" class="carousel slide">
            <!--<ol class="carousel-indicators">
            <li data-target="#countertopCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#countertopCarousel" data-slide-to="1"></li>
            <li data-target="#countertopCarousel" data-slide-to="2"></li>
            </ol>-->
            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="active item">
                    <picture>
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsE-l-min.jpg" media="(min-width: 1201px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsE-m-min.jpg" media="(min-width: 601px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsE-s-min.jpg" media="(max-width: 600px)" />
                        <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsE-l-min.jpg" />
                    </picture>
                </div>
                 <div class="item">
                    <picture>
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsF-l-min.jpg" media="(min-width: 1201px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsF-m-min.jpg" media="(min-width: 601px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsF-s-min.jpg" media="(max-width: 600px)" />
                        <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsF-l-min.jpg" />
                    </picture>
                </div>
                <div class="item">
                    <picture>
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsA-l-min.jpg" media="(min-width: 1201px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsA-m-min.jpg" media="(min-width: 601px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsA-s-min.jpg" media="(max-width: 600px)" />
                        <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsA-l-min.jpg" />
                    </picture>
                </div>
                <div class="item">
                    <picture>
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsB-l-min.jpg" media="(min-width: 1201px)" />
                        <sorce srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsB-m-min.jpg" media="(min-width: 601px)" />
                            <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsB-s-min.jpg" media="(max-width: 600px)" />
                        <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsB-l-min.jpg" />
                    </picture>
                </div>
                <div class="item">
                    <picture>
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsC-l-min.jpg" media="(min-width: 1201px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsC-m-min.jpg" media="(min-width: 601px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsC-s-min.jpg" media="(max-width: 600px)" />
                        <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsC-l-min.jpg" />
                    </picture>
                </div>
                <div class="item">
                    <picture>
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsD-l-min.jpg" media="(min-width: 1201px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsD-m-min.jpg" media="(min-width: 601px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/mouldingsD-s-min.jpg" media="(max-width: 600px)" />
                        <img src="<?php bloginfo('template_directory'); ?>/images/mouldingsD-l-min.jpg" />
                    </picture>
                </div>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#mouldingCarousel" data-slide="prev"><img src="<?php bloginfo('template_directory'); ?>/images/pg-left.svg"></a>
            <a class="carousel-control right" href="#mouldingCarousel" data-slide="next"><img src="<?php bloginfo('template_directory'); ?>/images/pg-right.svg"></a>
        </div>
        <div class="complement-doors-windows">
            <header>
                <h2>Complement Your Custom Millwork</h2>
                <p>Complement your custom millwork with Kirkwood Stair &amp; Millwork’s additional services. Grand millwork accentuates the natural beauty of domestic and exotic hardwoods, enhancing your space. Our team of designers, installers, craftsmen, and wood carvers can help you design timeless architectural stairs, natural wood countertops, and custom windows and doors to accompany your millwork.</p>
            </header>
            <div class="container">
                <div class="callout">
                    <div class="image">
                        <img src="<?php bloginfo('template_directory'); ?>/images/wood-countertop-callout.jpg" alt="Wood Countertop Callout" />
                    </div>
                    <div class="text">
                        <h3>Wood Countertops</h3>
                        <p>Discover the beauty and durability of natural wood countertops for your kitchen, islands, and bars. All of our wood countertops are completely hand sanded, then protected and finished with Waterlox® tung oil to create a lasting, lustrous and food-prep safe surface.</p>
                        <p><a href="/wood-countertops/">More about wood countertops &gt;</a></p>
                    </div>
                </div>
                <div class="callout">
                    <div class="image opp">
                        <img src="<?php bloginfo('template_directory'); ?>/images/stairs-callout.jpg" alt="Stairs Callout" />
                    </div>
                    <div class="text opp">
                        <h3>Stairs</h3>
                        <p>No other interior architectural element makes a stronger statement than your staircase. Whether you’re interested in new stairs and stair parts or enhancing your existing stairs, we will help you design, build and install the best custom staircase for your space.</p>
                        <p><a href="/stairs/">More about stairs &gt;</a></p>
                    </div>
                </div>
                <div class="callout">
                    <div class="image">
                        <img src="<?php bloginfo('template_directory'); ?>/images/windows-doors-callout.jpg" alt="Windows and Doors Callout" />
                    </div>
                    <div class="text">
                        <h3>Windows &amp; Doors</h3>
                        <p>Customization is vital to making your space unique. That’s why we are a proud distributor of Kolbe Windows &amp; Doors, an industry leader in custom windows and doors designed to fit any unique vision. You bring the idea, and our knowledgeable staff will help you choose the right Kolbe products for your project.</p>
                        <p><a href="/doors-windows/">More about windows &amp; doors &gt;</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="call-out-bar" style="margin-top:0;">
            <p><a href="/contact">Interested in custom millwork? Click here for an estimate.</a></p>
        </div>



	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

  <?php endwhile; // end of the loop.*/  ?>
</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>