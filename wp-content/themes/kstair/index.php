<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package KStair
 */

get_header(); ?>

<!-- Featured Background-->
		<div id="top"></div>
		<div class="fullscreen" id="home-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <img src="<?php bloginfo('template_directory'); ?>/images/kstair-logo.png">
        <p>We’ve crafted custom and architectural millwork since 1899. It’s our passion, and that shows in the quality, detail and soul we put into every single woodwork project. </p>
		<div class="scrolling-arrow">
        <p class="center"><a href="#scrolling-content">Scroll Down</a></p>
        <div class="bounce"><a href="#scrolling-content"><img class="animate" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a></div>
        </div>
        </div>
        </div>
        </div>
        
        <div id="scrolling-content"></div>

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
        	<ph>314.781.5151</ph>
            <p class="italic">Contact us with any questions and comments.</p>
            	<a href="/contact">
                <div class="contact-btn">
            	<p>Request a Quote</p>
                </div>
                </a>
        	</div>
        </div>
        </div><!-- end of .action bar -->
        
        <div id="home-A">
        <div class="container">
        <div class="home-page-text">
        <h2>Wood Countertops</h2>
        <p>Wood countertops deliver an incredible presence and balance to any space. Discover why so many designers are turning to the beauty and durability of natural wood countertops for kitchen, islands, and bars. </p>
        <p><a href="/wood-countertops">Click here for more information &gt;</a></p>
        </div>
        </div>
        </div>

        <div id="home-B">
        <div class="container">
        <div class="home-page-text">
        <h2>What We Do</h2>
        <p>Kirkwood Stair &amp; Millwork has a deep, rich history of designers, craftsmen, wood carvers, and installers who create handcrafted, custom millwork - from stunning stairs to sleek wood countertops -- to your precise specifications. We also design, handcraft and build balusters, mouldings, arched casings, curved crowns, mantles, bookcases, library paneling, hand-hewn beams and doors. All of our offerings feature complete design, engineering and installation services, and are a thoroughly excellent value. </p>
        </div>
        </div>
        </div>
        
        <div id="home-C">
        <div class="container">
        <div class="home-page-text">
        <h2>We Put Our Best Woodwork Forward</h2>
        <p>Our builder, contractor, homeowner and designer clients rely on us to make their woodwork concepts and plans come to life. Our company has a cool culture - one that inspires each of us to do our best work with precision, timeliness and artfulness in our effort to always exceed expectations. One of our greatest rewards, truly, is to see how happy we’ve made our clients.</p>
        </div>
        </div>
        </div>
        
        <div id="home-D">
        <div class="container">
        <div class="home-page-text">
        <h2 class="bold">Our Design Center</h2>
        <p>Ours is a one-stop design center, a cutting-edge concept center that's capable of producing a range of looks from traditional to contemporary. During the design and concept phase, our sales staff, designers, and support staff work with your builder, architect, and designer to translate your ideas from paper possibilities into digital realities.</p>
        <p class="italic">This collaborative approach helps ensure your design is expertly cared for and understood by craftsmen who build your countertops, stairs, millwork, doors, and windows.</p>
        </div>
        </div>
        </div>
        
        <div id="home-E">
        <div class="container">
        <a href="/our-work"><img src="<?php bloginfo('template_directory'); ?>/images/big-cedar-callout.png"></a>
        <div class="home-page-text">
        <h2 class="italic bold"><a href="/our-work">Big Cedar</a></h2>
        <p class="bold"><a href="/our-work">Check out how we constructed and installed the grand staircase in the Big Cedar Lodge in Branson, MO &gt;</a></p>
        </div>
        </div>
        </div>
        
        <div id="home-F">
        <div class="container">
        <div class="home-page-text">
        <h2>Craftsmanship</h2>
        <p>Once the design is created, we pursue the best domestic hard woods (with an eye towards sustainability), like Red and White Oak, Poplar, Steamed Walnut and Cherry as well as exotics such as Mahogany, Jatoba, Tigerwood, and even Bamboo. We purchase all of our lumber from mills practicing sustainable and environmental responsibility.</p>
        <p>When the raw lumber arrives, our craftsmen blend old world joineries like mortise and tenon, and dove- tailing with the most advanced woodworking equipment in the world to create unique, architectural features. We create all of our countertops, stairs, millwork, and doors at our plant in Pacific, Missouri. </p>
        </div>
        </div>
        </div>
        
        
        
        <div class="call-out-bar">
        <p><a href="/contact">Questions? Comments? Need a quote? Click here to contact us.</a></p>
        </div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
