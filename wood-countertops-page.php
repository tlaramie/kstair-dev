<?php /*Template Name: Wood Countertops*/?>

<?php get_header(); ?>

<!-- Featured Background-->
		<div id="top"></div>
		<div class="fullscreen" id="countertops-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <h1 class="featured-title italic">Wood Countertops</h1>
        <p>Wood is natural and timeless, warm and relaxing, with a gentle, irresistible appeal for any decor. Wood's beautiful, infinite grains and colors create endless design options in custom countertops for builders, contractors and homeowners.</p>
        <p>To see examples of our custom wood countertops, click <a href="#countertopCarousel" class="bold">here.</a></p>
        <div class="scrolling-arrow">
        	<p class="center"><a href="#scrolling-content">Scroll Down</a></p>
        	<a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
        </div>
        </div>
        </div>
        </div>

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <div id="scrolling-content"></div>
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
                <a href="http://kstairquote.com/Quote/Step1" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/estimator-logo.svg" alt="Kirkwood Stair Quoting Engine"></a>
                <p class="italic" style="padding-bottom:0">Get an estimate for your custom wood countertop.</p>
            </div>
        </div>
        </div><!-- end of .action bar -->
        
		<div id="why-wood">
        <div class="container">
        <div class="intro-text">
        <h2>Why Wood?</h2>
        <p>Wood countertops bring incredible presence and balance to any space. All of our wood countertops are completely hand sanded, then protected and finished with Waterlox<sup>&reg;</sup> tung oil to create a lasting, lustrous and food-prep safe surface. Discover why so many designers and homeowners are choosing the beauty and durability of natural wood countertops for kitchens, islands and bars.</p>
        </div>
        <div class="icon-callouts">
        <div>
        	<a href="/our-work"><img src="<?php bloginfo('template_directory'); ?>/images/countertop-icon.svg" alt="Wood Countertop Process"></a>
        	<p class="bold"><a href="/our-work">See how our countertops are made from start to finish.</a></p>
            </div>
<div>
        	<a href="/warranty"><img src="<?php bloginfo('wpurl'); ?>/wp-content/uploads/2016/03/warranty-icon.svg" alt="Warranty Icon"></a>
        	<p class="bold"><a href="/warranty">Check out our lifetime warranty.</a></p>
            </div>
            <div>
        	<a href="http://www.pinterest.com/kirkwoodstair/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/pinterest-icon.svg" alt="Follow us on Pinterest"></a>
        	<p class="bold"><a href="http://www.pinterest.com/kirkwoodstair/" target="_blank">Follow us on Pinterest to see custom wood countertops and more.</a></p>
            </div>
        </div>
        </div>
        </div>
        
        <div id="wood-advantages">
        <div class="container">
        <h2>Wood Countertop <span class="bold">Advantages</span></h2>
        	<div>
            <img src="<?php bloginfo('template_directory'); ?>/images/liquids.svg" alt="Liquid Resistant">
            <p class="bold">Liquid Resistant</p>
            <p>Easy to Clean</p>
            </div>
            <div>
            <img src="<?php bloginfo('template_directory'); ?>/images/durable.svg" alt="Durable">
            <p class="bold">A Living Finish</p>
            <p>Unlike other hard surface finishes that would have to be replaced</p>
            </div>
            <div>
            <img src="<?php bloginfo('template_directory'); ?>/images/glass-wear-friendly.svg" alt="Glassware Friendly">
            <p class="bold">Glassware Friendly</p>
            <p>Naturally smooth and soft</p>
            </div>
            <div>
            <img src="<?php bloginfo('template_directory'); ?>/images/food-safe.svg" alt="Food Prep Safe">
            <p class="bold">Safe</p>
            <p>Low VOC and non-toxic with Waterlox<sup>&reg;</sup> tung oil</p>
            </div>
            <div>
            <img src="<?php bloginfo('template_directory'); ?>/images/enduring-beauty.svg" alt="Lasting Beauty">
            <p class="bold">Lasting Beauty</p>
            <p>With rich grains, colors, and gorgeous natural aging</p>
            </div>
            <div>
            <img src="<?php bloginfo('template_directory'); ?>/images/easy-repair.svg" alt="Easy to Repair">
            <p class="bold">Easy to Maintain &amp Repair</p>
            <p>Can be sanded and re-stained</p>
            </div>
            <div>
            <img src="<?php bloginfo('template_directory'); ?>/images/easy-clean.svg" alt="Easy to Clean and Wipe">
            <p class="bold">Easy to Clean</p>
            <p>Can be protected with a variety of finishes</p>
            </div>
            <div>
            <img src="<?php bloginfo('template_directory'); ?>/images/cost-effective.svg" alt="Cost-Effective">
            <p class="bold">Cost Effective</p>
            <p>Almost always less than granite countertops</p>
            </div>  
        </div>
         <div id="pdf-download">
         	<p class="bold">&nbsp;To see endless design options and styles and for more information on our wood countertops,</p>
        	<a href="<?php bloginfo('template_directory'); ?>/pdf/woodcountertoppdf.pdf" target="_blank">
            <div id="pdf-btn">
        	<p>Download a PDF</p>
            </div>
            </a>
            <a href="<?php bloginfo('template_directory'); ?>/pdf/wood-countertop-booklet.pdf" target="_blank">
            <div id="pdf-btn">
            <p>Download a Booklet</p>
            </div>
            </a>
        </div>
        <div class="trees">&nbsp;</div>      
        </div>
        
        <div id="countertop-green">
        <div class="container">
        <img src="<?php bloginfo('template_directory'); ?>/images/globe.svg" alt="Renewable & Biodegradable">
        <p class="bold">Renewable and Eco-Friendly</p>
        <p class="italic">Wood is a replenishing, renewable resource unlike granite, which must be mined.</p>
        </div>
        </div>
        <div id="countertop-dark-green">
        <div class="container">
        <p class="bold"><a href="/sustainability">Click here to learn more about our sustainable practices.</a></p>
        </div>
        </div>
        
    <div id="countertopCarousel" class="carousel slide">
    <!-- Carousel items -->
    <!-- Insert slide names into the data-slides array. Make sure each image has a large, medium, and small per the naming convention below -->
    <div class="carousel-inner lazy-slider" data-slides='["countertopH", "countertopA", "countertopB", "countertopC", "countertopD", "countertopE", "countertopF", "countertopG"]'>
        <div class="active item">
            <picture>
                <source srcset="<?php bloginfo('template_directory'); ?>/images/lazy-load-carousel-placeholder-l.gif" media="(min-width: 1201px)" />
                <source srcset="<?php bloginfo('template_directory'); ?>/images/lazy-load-carousel-placeholder-m.gif" media="(min-width: 601px)" />
                <source srcset="<?php bloginfo('template_directory'); ?>/images/lazy-load-carousel-placeholder-s.gif" media="(max-width: 600px)" />
                <img src="<?php bloginfo('template_directory'); ?>/images/lazy-load-carousel-placeholder-l.gif" />
            </picture>
        </div>
    </div>
    <!-- Carousel nav -->
    <a class="carousel-control left" href="#countertopCarousel" data-slide="prev"><img src="<?php bloginfo('template_directory'); ?>/images/pg-left.svg"></a>
    <a class="carousel-control right" href="#countertopCarousel" data-slide="next"><img src="<?php bloginfo('template_directory'); ?>/images/pg-right.svg"></a>
    </div>
                
        
		<div class="call-out-bar">
        <p><a href="http://kstairquote.com/Quote/Step1" target="_blank">Interested in a new wood countertop? Click here for an estimate.</a></p>
        </div>
        
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
