<?php /*Template Name: Countertop Quote*/?>


<?php get_header(); ?>


		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <div id="scrolling-content"></div>
        
        <div id="quote-content">
         <?php echo do_shortcode('[advanced_iframe securitykey="ecb8f0254db00a9c3f4a24b2cae3a289b6855bfa" use_shortcode_attributes_only="true" src="//www.kstairquote.com" width="100%" height="2500px" scrolling="no" id="advanced_iframe" hide_page_until_loaded="true" show_iframe_loader="true" enable_external_height_workaround="true" hide_page_until_loaded_external="true" ]'); ?>       
       </div>
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
