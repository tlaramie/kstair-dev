<?php /*Template Name: Our Work - Custom Countertops*/?>

<?php get_header(); ?>

<!-- Featured Background-->
        <div id="top"></div>
		<div class="fullscreen" id="work-countertops-feature">
        <div class="feature-text">
        <div class="feature-text-wrapper">
        <h1 class="featured-title italic">Mahogany and Brazilian Cherry Custom Countertops</h1>
        <p><em>"This beautiful custom-built kitchen is detailed with rich mahogany and cherry wood cabinets and trim, with Brazilian cherry floors. The owner wanted custom Brazilian cherry wood kitchen countertops that would compliment and blend closely with the cabinets, trim, and floors. In the lower level, the owner wanted a custom mahogany countertop positioned in a stone arched opening. See the attention to detail our staff puts in to every top, at every process."</em></p>
        <img src="<?php bloginfo('template_directory'); ?>/images/brian-signature-sm-white.png"> 
        <p>President<br/>Kirkwood Stair &amp; Millwork</p>
		<div class="scrolling-arrow">
        	<p class="center"><a href="#scrolling-content">Scroll Down</a></p>
        	<a href="#scrolling-content"><img class="bounce" src="<?php bloginfo('template_directory'); ?>/images/pg-down.svg"></a>
        </div>
        </div>
        </div>
        </div>
        
        <div id="scrolling-content"></div>

		<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        
        <!-- Action Bar -->
        <div class="action-bar" id="action-bar">
        <div class="container">
        	<div class="newsletter-ad">
        	<p class="subscribe">Subscribe</p>
            <p class="italic">Stay in touch and receive exclusive offers.</p>
			<button type="button" class="newsletter-btn" data-toggle="collapse" data-target="#newsletter-signup">Sign Up Here</button>
			<div id="newsletter-signup" class="collapse"><?php gravity_form(1, false, false, false, false, false, false); ?></div>
        	</div>
        	<div class="contact-ad">
                <a href="http://kstairquote.com/Quote/Step1" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/estimator-logo.svg" alt="Kirkwood Stair Quoting Engine"></a>
                <p class="italic">Get an estimate for your custom wood countertop.</p>
            </div>
        </div>
        </div><!-- end of .action bar -->
        
        <div id="project-categories">
<svg class="current" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 90 90" enable-background="new 0 0 90 90" xml:space="preserve">
<circle fill="#C7B299" cx="45" cy="45.2" r="44"/>
<g>
	<path fill="#42322B" d="M67.2,24.4H21.9L7.1,58h75.9L67.2,24.4z M23.6,54.7l5.4-18h21l3.3,18H23.6z M57.7,47.6h-2.3v-2.9h-2.1v-2.9
		h2.2V31.3c0-1.3-1.4-2.6-3.5-2.6c-2.1,0-3.3,1.4-3.3,3.3l-1.9,0c0-1.1,0.6-5.9,5.1-5.9c4.5,0,5.8,2.9,5.8,4.7V47.6z"/>
	<rect x="7.1" y="58.9" fill="#42322B" width="75.9" height="1.8"/>
</g>
</svg>

<a href="index.php?page_id=166"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 90 90" enable-background="new 0 0 90 90" xml:space="preserve">
<circle fill="#C7B299" cx="45" cy="45.2" r="44"/>
<g>
	<polygon fill="#42322B" points="26.3,18.8 24.7,21.5 65.1,21.5 63.8,18.8 	"/>
	<rect x="24.7" y="21.9" fill="#42322B" width="40.5" height="5.9"/>
	<polygon fill="#42322B" points="23.7,29.5 21.8,32.5 67.8,32.5 66.3,29.5 	"/>
	<rect x="21.8" y="33" fill="#42322B" width="46.1" height="6.7"/>
	<polygon fill="#42322B" points="20.5,41.4 18.3,45 71.4,45 69.7,41.4 	"/>
	<rect x="18.3" y="45.6" fill="#42322B" width="53.1" height="7.7"/>
	<polygon fill="#42322B" points="18.2,55.2 15.8,59.1 74.6,59.1 72.7,55.2 	"/>
	<rect x="15.8" y="59.9" fill="#42322B" width="58.9" height="8.2"/>
</g>
</svg></a>

<!--<a href="https://www.pinterest.com/kirkwoodstair/" target="_blank">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 120 120" enable-background="new 0 0 120 120" xml:space="preserve">
<circle fill="#C7B299" cx="60" cy="60.3" r="58.7"/>
<g>
	<path fill="#43332B" d="M56.4,69.7c-0.5,2.1-1.1,3.9-1.4,5.8c-1.1,5.3-3.4,9.9-6.8,14.3c-0.1,0.1-0.1,0.2-0.2,0.3
		c-0.2,0.1-0.4,0.2-0.5,0.2c-0.1,0-0.2-0.3-0.3-0.4c-0.2-1.7-0.4-3.5-0.5-5.3c-0.3-4.1,0.7-8,1.6-11.9c1.1-4.5,2.2-8.9,3.2-13.4
		c0.1-0.3,0-0.8-0.1-1.1c-1-2.9-1.2-6-0.1-8.8c0.8-2.2,2.1-3.9,4.5-4.6c3.2-0.9,5.8,1.1,5.9,4.3c0.1,2.1-0.4,4-1,6
		c-0.7,2.3-1.4,4.5-2,6.8c-0.9,3.7,2,7,5.8,6.6c2.9-0.2,5-1.9,6.6-4.1c2-2.7,2.8-5.8,3.4-8.8c0.3-2.3,0.5-4.5,0.3-6.8
		c-0.7-5.6-3.9-9.4-9.3-10.7c-5.9-1.5-11.5-0.7-16.1,3.5c-4.5,4-6.1,8.9-5.1,14.9c0.2,1.4,1,2.7,2,3.9c0.5,0.7,0.8,1.3,0.4,2.1
		c-0.2,0.8-0.4,1.6-0.7,2.4c-0.3,1.1-0.9,1.3-1.9,0.9c-2.7-1.2-4.6-3.3-5.6-6.1C36,53,37.1,46.7,41,41.3c3.5-5,8.3-7.9,14.2-9.1
		c4.9-1,9.7-1,14.5,0.9c6.5,2.5,10.8,7,12.5,13.7c0.7,2.5,0.5,5.1,0.3,7.7c-0.4,3.8-1.3,7.4-3.3,10.6c-2.4,4.3-5.8,7.3-10.5,8.5
		c-2.8,0.8-5.8,0.9-8.4-0.3C58.8,72.4,57.5,71.4,56.4,69.7z"/>
</g>
</svg></a>-->
        
        <a href="https://www.pinterest.com/kirkwoodstair/" target="_blank"><div class="pinterest-call container" id="countertops-pinterest">
        <p>Follow us for more examples of our custom wood countertops.</p>
        </div></a>
        </div>
                       
        <div class="featured-gallery" id="countertop-gallery">
        	<div>
          		<div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopB-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopB-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopB-l.jpg"/>
                    </picture>
                    <div class="gallery-text-wrapper" id="countertop-slide1-text">
                    <div id="brown-text-bar">
            		<p>We developed our ideas for this project on paper and then created full-size templates to ensure the design was understood by our craftsmen.<br/><span class="bold">A detailed print-out of the final dimensions was used for lumber selection</span><br/> and to provide instructions for the CNC (Computerized Numerical Control) cutting machine operator.</p>
                    <br/><br/>
                    <p class="story-action-text">Continue scrolling to the right to see the whole process &gt;</p>
                    </div>
                	</div>
                </div>         
            	<div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopC-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopC-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopC-l.jpg"/>
                    </picture>
                    <div class="gallery-text-wrapper" id="stairs-slide2-text">
                    <div id="brown-text-bar">
            		<p>Starting with rough-sawn lumber, wood was <span class="bold">hand-selected for width and color match.</span></p>
                    </div>
                	</div>
                </div>
                <div class="featured-gallery-post text-block-brown">
                	<div class="gallery-text-wrapper">
             		<p>We sent the raw lumber through our Gang Rip machine to prepare the edges for laminating, and marked the boards for defects, knots, checks, and cracks. Then we sent it through the Optimizing Chop Saw.</p>
                	</div>
                </div>  
         		<div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopD-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopD-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopD-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post text-block-tan">
                	<div class="gallery-text-wrapper">
                	<p>Next, we applied glue to the edges of the boards and arranged them for width to best match the finished size.<br/><br/><span class="bold"> We carefully looked at the grain of each individual board, and alternated the faces up and down to give the countertop stability and strength.</span></p>
                	</div>
                </div> 
         		<div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopE-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopE-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopE-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post text-block-brown">
                	<div class="gallery-text-wrapper">
                	<p>After the glue was set, we transfered the countertop to the planer or wide belt sander to be flattened and smoothed. 
This sander has three different heads used to rough sand, medium sand, and finish sand the countertop.  The numbers located on the ends of the boards are used to indicate proper layout and sequencing from first selection to final assembly. Notice that the sequence has changed slightly. Boards are thoroughly inspected during each process and this new sequence was chosen for the best color and strength for this assembly.<br/><br/> <span class="bold"> This demonstrates the attention to detail we put into every countertop, at every process.</span></p>
                	</div>  
                </div>
				<div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopF-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopF-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopF-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopG-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopG-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopG-l.jpg"/>
                    </picture>
                    <div class="gallery-text-wrapper" id="countertop-slide2-text">
                    <div id="brown-text-bar">
            		<p>After rough sanding and flattening, the countertop was <span class="bold">ready for final milling</span> on the CNC router machine.</p>
                    <p>We prepared the 5-axis CNC router machine for layout by downloading the programming information from the Engineering department.</p>
                    </div>
                	</div>
                </div>
                 <div class="featured-gallery-post">
                 	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopH-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopH-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopH-l.jpg"/>
                    </picture>
                 </div>
                <div class="featured-gallery-post text-block-tan">
                    <div class="gallery-text-wrapper">
                	<p>The countertop was placed on the Nimi Grid table where the router automatically selected the proper bits and cutting to program. Notice that the countertop sits above the aluminum table. Vacuum pods hold it in place and prevent the router bit from digging into the aluminum table. See <span class="bold">the bowed outside perimeter of the countertop taking shape</span> with the router bit.</p>
                    <p>Next, we prepared the metal sizing in order to weld a metal support bracket to the countertop.</p>
                	</div>
                </div>
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopI-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopI-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopI-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post text-block-brown">
                    <div class="gallery-text-wrapper">
                	<p><span class="bold">You may choose between a soft or crisp countertop edge.</span><br/><br/> If you prefer a soft edge, our craftsman will carefully ease the bottom edge of the countertop with seating to soften a sharp edge.</p>
                    <p>Next, we fine sanded the countertop profiled edge.</p>
                    <p>Then we applied the final orbital sanding process to the countertop prior to staining and finishing.</p>
                	</div>
                </div>
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopJ-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopJ-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopJ-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post text-block-tan">
                    <div class="gallery-text-wrapper">
                	<p>Once the countertop finish selection was made, we prepared the finish spraying process.</p>
                    <p>We sealed the countertop on all six sides – top, bottom, edges, and ends – <span class="bold">which is critical to stabilizing the countertop with the changing environs in a home.</span></p>
                	</div>
                </div>
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopK-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopK-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopK-l.jpg"/>
                    </picture>
                </div>
                 <div class="featured-gallery-post text-block-brown">
                 <div class="gallery-text-wrapper">
                 <p>A coat of clear, natural Tung Oil was applied to the countertop. Once the finish was applied and dry, <br/>the countertop was <span class="bold">carefully wrapped and shipped</span> to the job site for installation.</p>
                 </div>
                 </div>
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopL-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopL-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopL-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post text-block-tan">
                	<div class="gallery-text-wrapper">
             		<p>During the Brazilian cherry (Jatoba) kitchen countertop installation process, <span class="bold">we use hand joinery to chisel anchoring slots and apply blocking</span>.</p>
                    <p>Then we drilled holes in the cabinetry blocking, and ran fasteners through to the wood countertop.</p>
                    <p>We calculated the best pattern for screw holes to fasten brackets to the countertop and brackets to the cabinetry, and then drilled holes in the metal brackets.</p>
                	</div>
                </div> 
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopN-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopN-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopN-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post text-block-brown">
                	<div class="gallery-text-wrapper">
             		<p>In the lower level, the mahogany countertop was coped to the stone pillars, and the brackets were leveled and stabilized on all points of the stone and wood surfaces.</p>
                    <p>After carefully laying out, measuring, cutting, and fitting the countertop to the stone,<span class="bold"> we slid the countertop into place.</span></p>
                    <p>To complete the installation, we permanently fastened the countertop to the wall. </p>
                	</div>
                </div>
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopO-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopO-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopO-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post text-block-brown">
                	<div class="gallery-text-wrapper">
             		<p>Here are the custom countertops installed. In the kitchen, the <span class="bold">custom Brazilian cherry wood kitchen countertop</span> beautifully compliments the existing cabinets, trim, and floors.</p>
                    <p>In the lower level, the <span class="bold">custom mahogany countertop</span> is nestled within the stone arched opening leading to the kitchen area.</p>
                	</div>
                </div> 
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopP-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopP-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopP-l.jpg"/>
                    </picture>
                </div>
                <div class="featured-gallery-post">
                	<picture>
                    	<source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopQ-l.jpg" media="(min-width: 769px)" />
                        <source srcset="<?php bloginfo('template_directory'); ?>/images/work-countertopQ-s.jpg" media="(max-width: 769px)" />
                		<img src="<?php bloginfo('template_directory'); ?>/images/work-countertopQ-l.jpg"/>
                    </picture>
                </div                                     
            ></div>
        </div> <!--End of featured-gallery-->
       
	   <?php /* while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' );  ?>

		<?php endwhile; // end of the loop.*/  ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    

<?php get_footer(); ?>
